<?php

if ($_SERVER["REQUEST_METHOD"] == "POST"){

	$id = test_input($_POST["id"]);
	$proc = test_input($_POST["procedure"]);
	$name = test_input($_POST["name"]);
	$price = test_input($_POST["price"]);
	$comment = test_input($_POST["comment"]);
	$status = test_input($_POST["status"]);
	$supplier = test_input($_POST["supplier"]);
	$uri=test_input($_POST["uri"]);
	$resource=test_input($_POST["resource"]);
	$gresponse=$_POST["g-recaptcha-response"];
	$date = test_input($_POST["date"]);
	$fileDesc= test_input($_POST["dname"]);
	
	// Check whether we have a valid user
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array('secret' => '6Lf7_Q8TAAAAAOkqdoU4EYSzNH3GcMyaNsHqyllw', 'response' => $gresponse);
	
	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data),
		),
	);
	
	// Get the information from the google site verify service
	$context = stream_context_create($options);
	$gverify = file_get_contents($url, false, $context);
	
	$decodedgverify = json_decode($gverify);
	
	# Output information about allow_url_fopen:
	if (ini_get('allow_url_fopen') == 1) {
		# Do nothing
	} else {
		echo '<p style="color: #A00;">fopen is not allowed on this host. This form will not work.</p>';
	}

	if($decodedgverify->{"success"} == true){
		
		// Get the year from the date field
		$year = substr($date, 0, 4);
		
		// See if we can sort out the uploaded file (if there was one) first
		$targetPath = '';
		$uploadMessages = '';
		$failedUpload = false;
		if ($_FILES["userUploadedFile"]['size'] != 0){
			if (!addNewFile($fileDesc, $name, $year, $targetPath, $uploadMessages)) {
				// We failed to upload the file
				$failedUpload = true;
			}
		}
		
		// Add a node to the logfile
		if (!$failedUpload){
			addToLog($id, $comment, $resp, $date, $name, $resource, $targetPath, $proc, $status, $price, $supplier);
		}
		
		#
		# Redirect
		#
		echo "<html>";
		echo "<head>";
		echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
		if($id != ""){
			$redirectid=$id;
		}
		else{
			if($proc != ""){
				$redirectid=$proc;
			}
			else{
				$redirectid=$supplier;
			}
		}
		
		echo '<meta http-equiv="refresh" content="1;url=http://ubuc.org/maintenance/Manual.php?id=',$redirectid,'">';
		echo "</head>";
		echo "<body>";
		if (!$failedUpload){
			echo "<h1>Thank you for your input</h1>";
		} else {
			echo "<h1>New log failed</h1>";
			echo "<p>Have a look at the following message</p>";
			echo "<p>" . $uploadMessages ."</p>";
		}
		echo "<h2>Redirecting in 1 second</h2>";
		echo "</body>";
		echo "</html>";
		
	}else{
		echo "<html>";
		echo "<head>";
		echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
		echo "</head>";
		echo "<body><h1>We think you might be a robot!</h1>";
		echo '<img src="Resources/webbot.png"/>';
		echo "<h2>If you are a human</h2>";
		echo "Your log has been ignored as its assumed to be spam!<br/>"; 
		echo "Go back to the log page remember to click the box ";
		echo "indicating you are not a robot</body></html>"	;
	}

exit();
 
}else{
	$idparam=htmlspecialchars($_GET["id"]);
	$procparam=htmlspecialchars($_GET["proc"]);
	$supplierparam=htmlspecialchars($_GET["supplier"]);

	// define variables and set to empty values
	$price = $name = $comment= $status = $supplier = "";

}

function addNewFile($fileDesc, $uploaderName, $uploadYear, &$targetPathOut, &$messages){
	/* This function will try and add a new file to the database.
	 * Returns false if it failed to upload.
	 * Inputs:
	 * 		- $fileDesc 	  - A description of the file
	 * 		- $uploaderName   - The name of the person who uploaded it
	 * 		- $uploadYear     - The year that the comment relates to (as a string)
	 * Ouputs:
	 * 		- &$targetPathOut - The full path to the file
	 * 		- &$messages	  - A message about the upload
	 */
	
	// Check that the uploaded file array is not empty
	if (empty($_FILES)){
		// No file was uploaded
		$messages = "No files selected for uploading";
		return false;
	}
	
	// Define the acceptable file extensions (lower case)
	$allowedExts = array("gif", "jpg", "jpeg", "png", "doc", "docx", "pdf");
	
	// Figure out our target filename and path
	// Starts a new folder each year
	$relTargetPath = "/LogResources/" . $uploadYear;
	$targetPath = getcwd() . $relTargetPath;
	// If the folder does not exist then we make it
	if (!is_dir($targetPath)){
		mkdir($targetPath, 755);
	}
	
	// Define the target file
	$target_file = $targetPath . "/" . basename($_FILES["userUploadedFile"]["name"]);
	$rel_target_file = $relTargetPath . "/" . basename($_FILES["userUploadedFile"]["name"]);
	$targetFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	
	// Check if file already exists
	if (file_exists($target_file)) {
		$messages = "Sorry, file already exists.";
		return false;
	}
	
	// Check file size
	if ($_FILES["userUploadedFile"]["size"] > 30000) {
		$messages = "Sorry, your file is too large.";
		return false;
	}
	
	// Allow certain file formats
	if(!in_array($targetFileType, $allowedExts)) {
		$messages = "Unaccepted file type";
		return false;
	}
	
	// The checks have passed, so move the file to its target location
	if (move_uploaded_file($_FILES["userUploadedFile"]["tmp_name"], $target_file)) {
		$messages = "The file ". htmlspecialchars( basename( $_FILES["userUploadedFile"]["name"])). " has been uploaded.";
	} else {
		// The move went wrong
		$messages = "Something went wrong moving the file from its temporary location.";
		return false;
	}
	
	// Given that has all gone well, we now want to add the file to the uploaded files log
	$sxe=simplexml_load_file("xml/LogResources.xml");
	$newentry=$sxe->addchild("resource","");
	$newentry->addAttribute('loaded', date("Ymd"));
	$newentry->addAttribute('by', $uploaderName);
	$newentry->addAttribute('name', $fileDesc);
	$newentry->addAttribute('path', $target_file);
	$sxe->asXMl("xml/LogResources.xml");
	
	// Return true if it all worked out OK
	$targetPathOut = $rel_target_file;
	return true;
}

function addToLog($id, $comment, $resp, $date, $name, $resource, $targetPath, $proc, $status, $price, $supplier) {
	/* This function adds a new node to the log file 
	 * Inputs:
	 * 		- $id 			- The id of the item to add to
	 * 		- $comment 		- The test comment to add
	 * 		- $resp 		- ??
	 * 		- $date 		- The date the comment was made on
	 * 		- $name 		- The name of the person making the comment
	 * 		- $resource 	- The resorce to link to
	 * 		- $targetPath 	- The target path of an uploaded file
	 * 		- $proc 		- The procedure that was compleated
	 * 		- $status 		- The status of the item
	 * 		- $price 		- The price of doing this
	 * 		- $supplier 	- The supplier that was used
	 * */
	
	// Load the xml log file into an object
	$sxe=simplexml_load_file("xml/log.xml");
	
	// Create a new node in the xml object
	$newentry=$sxe->addchild("logentry",$comment.$resp);
	
	// Add the date to the new node
	$newentry->addAttribute('date', $date);
	
	// Add the persons name to the new node
	$newentry->addAttribute('name', $name);
	
	// If an item ID was provided, add this to the node
	if($id != ""){
		$newentry->addAttribute('id', $id);
	}
	
	// Add a resource to the new entry if one was provided
	if($resource != "none"){
		$newentry->addAttribute('uri', $resource);
	}
	elseif(!empty($targetPath)){
		// A file was uploaded, so associate it
		$newentry->addAttribute('uri', "http://ubuc.org/maintenance" . $targetPath);
	}
	elseif($uri != ""){
		$newentry->addAttribute('uri', $uri);
	}
	
	// If a procedure was marked as completed, then associate this with the node
	if($proc != ""){
		$newentry->addAttribute('procedure', $proc);
	}
	
	// If a status was provided, add this to the node
	if($status != "none"){
		$newentry->addAttribute('commenttype', $status);
	}
	
	// If a price was given, add this to the node
	if($price > 0){
		$newentry->addAttribute('price', $price);
	}
	
	// If a supplier was given, then associate this with the node
	if($supplier != "none"){
		$newentry->addAttribute('supplier', $supplier);
	}
	
	// Write the xml object to a file
	$sxe->asXMl("xml/log.xml");
	
	// Print anything that went wrong
	print_r(error_get_last());
	
	#
	# Format the xml
	#
	#$dom = new DOMDocument("1.0");
	#$dom->preserveWhiteSpace = false;
	#$dom->formatOutput = true;
	#$dom->loadXML($sxe->asXML());
	#$dom->save("xml/log2.xml");
}

?>


<?php
	function test_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	
	#LOAD XML FILE
	$doc = new DOMDocument();
	if (($idparam=="") and ($procparam=="")){
		# if the id and proc are not set its a comment against a supplier
		$doc->load('xml/suppliers.xml');
	}else{
		$doc->load('xml/UBUCManual.xml');
	}
	
	# START XSLT 
	$xslt = new XSLTProcessor(); 
	$XSL = new DOMDocument(); 
	$XSL->load( 'xslt/logentry.xslt', LIBXML_NOCDATA); 
	
	$xslt->importStylesheet( $XSL ); 
	
	#PRINT 
	$xslt->setParameter('', 'id', $idparam);
	$xslt->setParameter('', 'proc', $procparam);
	$xslt->setParameter('', 'supplier', $supplierparam);
	$xslt->setParameter('', 'CurrentDate', date("Y-m-d"));
	
	print $xslt->transformToXML( $doc );
	
	function transform($xml, $xsl) { 
	    $xslt = new XSLTProcessor(); 
	    $xslt->importStylesheet(new  SimpleXMLElement($xsl)); 
	    return $xslt->transformToXml(new SimpleXMLElement($xml));
	}
?>
