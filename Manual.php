<?php
// ----------------------------------------------------------------------------------------------------
// - Display Errors
// ----------------------------------------------------------------------------------------------------
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

// ----------------------------------------------------------------------------------------------------
// - Error Reporting
// ----------------------------------------------------------------------------------------------------
error_reporting(-1);

// ----------------------------------------------------------------------------------------------------
// - Shutdown Handler
// ----------------------------------------------------------------------------------------------------
function ShutdownHandler()
{
    if(@is_array($error = @error_get_last()))
    {
        return(@call_user_func_array('ErrorHandler', $error));
    };
    return(TRUE);
};
register_shutdown_function('ShutdownHandler');

// ----------------------------------------------------------------------------------------------------
// - Error Handler
// ----------------------------------------------------------------------------------------------------
function ErrorHandler($type, $message, $file, $line)
{
    $_ERRORS = Array(
        0x0001 => 'E_ERROR',
        0x0002 => 'E_WARNING',
        0x0004 => 'E_PARSE',
        0x0008 => 'E_NOTICE',
        0x0010 => 'E_CORE_ERROR',
        0x0020 => 'E_CORE_WARNING',
        0x0040 => 'E_COMPILE_ERROR',
        0x0080 => 'E_COMPILE_WARNING',
        0x0100 => 'E_USER_ERROR',
        0x0200 => 'E_USER_WARNING',
        0x0400 => 'E_USER_NOTICE',
        0x0800 => 'E_STRICT',
        0x1000 => 'E_RECOVERABLE_ERROR',
        0x2000 => 'E_DEPRECATED',
        0x4000 => 'E_USER_DEPRECATED'
    );
    if(!@is_string($name = @array_search($type, @array_flip($_ERRORS))))
    {
        $name = 'E_UNKNOWN';
    };
    return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
};
$old_error_handler = set_error_handler("ErrorHandler");
?>

<?php

// Bring in the required functions
require "procedureFunctions.php";

// Assign 'id' variable passed by the GET method to a local var
if (empty($_GET)){
	// Give an empty value to the parameter
	$idparam = '';
}
else{
	// Assign from the GET parameter array
	$idparam=htmlspecialchars($_GET["id"]);
}

// Run the daemon check function
damonCheckUpdate();

// Check if the passed ID was a procedure
if (idIsProcedure($idparam)){
	// Display the procedure page
	displayProcedure($idparam);
} else {
	// Original code

	// If the passed parameter was NOT == log...
	if($idparam!="log"){
	
		#LOAD XML FILES
		$doc = new DOMDocument();
		$doc->load('xml/UBUCManual.xml');
	
		# Pass one
		# This takes the UBUCManual.xml and adds a status and current value to each <item/>
		# ManualPass1.xslt contains the reference to log.xml
		$pass = new XSLTProcessor();
		$passxsl = new DOMDocument();
		$passxsl->load( 'xslt/ManualPass1.xslt', LIBXML_NOCDATA); 
		$pass->importStylesheet( $passxsl );
		$pass->setParameter('', 'id', $idparam);
		$doc2 = new DOMDocument();
		$doc2->loadXMl($pass->transformToXML( $doc ));
		
		# START XSLT 
		$xslt = new XSLTProcessor(); 
		$XSL = new DOMDocument(); 
	
		switch ($idparam) {
			case "sheet":
				$XSL->load( 'xslt/SheetDisplay.xslt', LIBXML_NOCDATA); 
				break;
			case "stock":
				$XSL->load( 'xslt/StockTake.xslt', LIBXML_NOCDATA); 
				break;
			case "planning":
				$XSL->load( 'xslt/Upcoming.xslt', LIBXML_NOCDATA); 
				break;
			case "add":
				$XSL->load( 'xslt/addItem.xslt', LIBXML_NOCDATA);
				break;
			default:
				$XSL->load( 'xslt/UBUCManual.xslt', LIBXML_NOCDATA); 
		}
		$xslt->importStylesheet( $XSL ); 
	
		#PRINT 
		$xslt->setParameter('', 'id', $idparam);
		$xslt->setParameter('', 'CurrentDate', date("Y-m-d"));
		print $xslt->transformToXML( $doc2 );
		
	}
	else
	{
		// The passed parameter was 'log'. Display the full log data
		
		// Setup a DOMDocument and load the log
		$doc2 = new DOMDocument();
		$doc2->load('xml/log.xml');
	
		# START XSLT 
		$xslt = new XSLTProcessor(); 
		$XSL = new DOMDocument(); 
		$XSL->load( 'xslt/logDisplay.xslt', LIBXML_NOCDATA); 
		$xslt->importStylesheet( $XSL ); 
	
		#PRINT 
		$xslt->setParameter('', 'id', $idparam);
		$xslt->setParameter('', 'CurrentDate', date("Y-m-d"));
		print $xslt->transformToXML( $doc2 );
	}
}

function transform($xml, $xsl) { 
    $xslt = new XSLTProcessor(); 
    $xslt->importStylesheet(new  SimpleXMLElement($xsl)); 
    $xslt->setParameter('', 'asset', 'Universe');
    return $xslt->transformToXml(new SimpleXMLElement($xml));
}

function damonCheckUpdate(){
	/* This function will check if the daemon has been run today, and if not, it will run it */ 
	
	// Open the text file with the date of last run
	if (file_exists("DaemonLogs.txt")) {
		$dLogs = fopen("DaemonLogs.txt", "r+");
		// Get the date from the file
		$lDateS = fgets($dLogs);
		$lDate = DateTime::createFromFormat("Y-m-d", $lDateS);
		// Overwrite with todays date
		rewind($dLogs);
		fwrite($dLogs, date("Y-m-d"));
	} else {
		$dLogs = fopen("DaemonLogs.txt", "w");
		fwrite($dLogs, date("Y-m-d"));
		
		// Pretend we got the last date as yesterday
		$lDate = DateTime::createFromFormat("Y-m-d", date("Y-m-d"));
		date_modify($lDate, '-1 day');
	}
	
	// Get todays date
	$nowDate = DateTime::createFromFormat("Y-m-d", date("Y-m-d"));
	
	// If the date is before today or empty then run the Daemon
	if ($lDate < $nowDate){
		// Run the Daemon
		runDaemon();
	}
	
	// Close the file
	fclose($dLogs);
}

function runDaemon(){
	/* This function will run the Daemon when called */
	
	// Run a pass over the manual to add statuses to everything
	// I think the Daemon may work without this bit
	$doc = new DOMDocument();
	$doc->load('xml/UBUCManual.xml');
	$pass = new XSLTProcessor();
	$passxsl = new DOMDocument();
	$passxsl->load( 'xslt/ManualPass1.xslt', LIBXML_NOCDATA);
	$pass->importStylesheet( $passxsl );
	$doc2 = new DOMDocument();
	$doc2->loadXMl($pass->transformToXML( $doc ));
	
	$xslt = new XSLTProcessor();
	$XSL = new DOMDocument(); 
	
	$XSL->load( 'xslt/UBUCDaemon.xslt', LIBXML_NOCDATA);
	$xslt->importStylesheet( $XSL );
	$xslt->setParameter('', 'CurrentDate', date("Y-m-d"));

	// Process the daemon
	$daemonOutput = $xslt->transformToXML( $doc2 );
	$daemonOutputDOM = new DOMDocument();
	$daemonOutputDOM->loadXML($daemonOutput);
	
	// Load the log file
	$logAsDOM = new DOMDocument();
	$logAsDOM->load('xml/log.xml');
	
	// Select all of the new entries
	$nodes = $daemonOutputDOM->getElementsByTagName("logentry");
	
	if (count($nodes)>0){
		for ($x=0; $x<count($nodes); $x++) {
			// Import and append the new entries to the log
			$thisNode = $nodes[$x];
			$thisNode = $logAsDOM->importNode($thisNode, true);
			$logAsDOM->documentElement->appendChild($thisNode);
		}
		
		// Overwrite the existing log file
		$logAsDOM->formatOutput = true;
		$logAsDOM->save("xml/log.xml");
	}else{
		// Nothing
	}
	
}

?>

