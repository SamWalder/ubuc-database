<?php
/*
 * This file allows the user to add new items to the database or edit existing ones
 * Sam Walder - 2020
 */

// Bring in the required functions
require 'xmlFunctions.php';

// If the server has got to this page through the "POST" method it means the "Submit" button on the form was clicked.
if ($_SERVER["REQUEST_METHOD"] == "POST"){

	// Assign the items that were posted to the form to variables
	$id   = scrub($_POST["procID"]);		// The item ID we are going to use
	$group = scrub($_POST["collection"]);
	$name = scrub($_POST["editName"]);			// The name of the item

	// Properties
	$manufacturer = scrub($_POST["manufacturer"]);
	$mass         = scrub($_POST["mass"]);
	$material     = scrub($_POST["material"]);
	$newvalue     = scrub($_POST["newvalue"]);
	$serial       = scrub($_POST["serial"]);
	$year         = scrub($_POST["year"]);
	$stStage      = scrub($_POST["stStage"]);
	$model        = scrub($_POST["model"]);
	$size         = scrub($_POST["size"]);

	// If they passed the robot test...
	if(isHuman() || isLocalhost()){

		// Load the xml manual file into the object
		$xml = simplexml_load_file("xml/UBUCManual.xml");
		
		// Select the item node (if it exists)
		$itemNode = $xml->xpath(sprintf('//item[@id="%s"]', $id));
		
		// If it is empty we need to make a new node in the correct group
		if (empty($itemNode)){
			// Select the group using the ID provided
			$itemGroup = $xml->xpath(sprintf('//itemgroup[@id="%s"] | //collection[@id="%s"]', $group, $group));
			
			// Get a new unique id based on this groups values
			// Select the last item in the group
			$groupItem = $itemGroup[0]->xpath('child::item[last()]');
			if(!empty($groupItem)){
				// Select the id of the last item
				$lastID = $groupItem[0]['id'];
				
				// Get a ew ID based on the last ID
				$id = getNewID($lastID);
			}else{
				// We did not find any items - perhaps an empty group
				// Figure out an item id
				$twoLetters = strtolower(substr($name, 0, 2));
				$id = getNewID($twoLetters . "001");
			}
			
			// Create the new child node in this group
			$itemNode = $itemGroup[0]->addChild("item");
			
			// Add the ID
			$itemNode->addAttribute("id", $id);
			
			$itemNode = $itemNode[0];
		} else {
			// We need to make sure it is in the correct itemgroup
			
			// Get the current parent node ID
			$parentNode = $xml->xpath(sprintf('//item[@id="%s"]/parent::*', $id));
			$parentID = $parentNode[0]['id'];
			
			// If they don't match we need to move our node
			if (strcmp($group, $parentID) !== 0 ){
				// Store the node content
				$nodeChild = $itemNode[0]->children();
				
				// Delete the old node
				$xml = deleteNode($xml, 'item', $id);
				
				// Select the group using the ID provided
				$itemGroup = $xml->xpath(sprintf('//itemgroup[@id="%s"] | //collection[@id="%s"]', $group, $group));
				
				// Create the new child node in this group
				$itemNode = $itemGroup[0]->addChild("item");
				
				// Add the ID
				$itemNode->addAttribute("id", $id);
				
				// Put the node content back in
				foreach($nodeChild as $child){
					$newChild = $itemNode->addChild($child->getName(), $child[0]);
					foreach($child->attributes() as $atName => $atVal){
						$newChild->addAttribute($atName, $atVal);
					}
				}
				
			}
			$itemNode =  $itemNode[0];
		}
		
		// Add or update each of the default parameters
		if (!empty($name)){
			$itemNode = editXMLAttribute($itemNode, 'name', $name);
		}
		if (!empty($manufacturer)){
			$itemNode = editXMLAttribute($itemNode, 'manufacturer', $manufacturer);
		}
		if (!empty($mass)){
			$itemNode = editXMLAttribute($itemNode, 'mass', $mass);
		}
		if (!empty($material)){
			$itemNode = editXMLAttribute($itemNode, 'material', $material);
		}
		if (!empty($newvalue)){
			$itemNode = editXMLAttribute($itemNode, 'newvalue', $newvalue);
		}
		if (!empty($serial)){
			$itemNode = editXMLAttribute($itemNode, 'serial', $serial);
		}
		if (!empty($year)){
			$itemNode = editXMLAttribute($itemNode, 'year', $year);
		}
		if (!empty($stStage)){
			$itemNode = editXMLAttribute($itemNode, 'stStage', $stStage);
		}
		if (!empty($model)){
			$itemNode = editXMLAttribute($itemNode, 'model', $model);
		}
		if (!empty($size)){
			$itemNode = editXMLAttribute($itemNode, 'size', $size);
		}
		
		// Now add any custom properties that were added
		for ($paramNo=1; $paramNo<9; $paramNo++){
			$thisProp = scrub($_POST["customParamName" . $paramNo]);
			$thisVal = scrub($_POST["customParamVal" . $paramNo]);
			
			// Edit the value if it is not empty
			if (!empty($thisProp) && !empty($thisVal)){
				$itemNode = editXMLAttribute($itemNode, $thisProp, $thisVal);
			}
		}
		
		// Now see if there were any other keys passed
		// Loop through everything that was posted
		foreach($_POST as $postName => $postVal){
			// If the post name starts with 'exist_' it is a custom param
			if(strcmp(substr($postName, 0, 6), "exist_") == 0){
				if(empty($postVal)){
					// If the value is empty we will remove the attribute
					$itemNode = removeXMLAttribute($itemNode, scrub(substr($postName, 6)));
				}else{
					// If the value is not empty, we will edit the attribute
					$itemNode = editXMLAttribute($itemNode, scrub(substr($postName, 6)), scrub($postVal));
				}
			}
		}

		// Write the xml object to a file
		$xml->asXMl("xml/UBUCManual.xml");
		
		// Print anything that went wrong
		print_r(error_get_last());
		
		/* Create the confirmation that the input was recorded */
		printConfirmationPage($id, $name);
		
	}else{
		// Print a page to accuse them of being a robot
		printRobotPage();
	}

	// Stop the execution of this file. I don't think this is needed now as I am using the if else properly.
	exit();
 
}else{
	// In the case we got to this form by any method other than "POST" then we render the edit item form

	// If there is something in the get field then we are going to edit an existing item
	// Otherwise, we are making a new one
	if (empty($_GET)){
		// Setup for editing a new item
		$itemName = "New Item";
		$originalCollection = "None";
		$idparam = 'null';
		$itemNode = "";
		
		// Set a flag
		$newItem = true;
	}else{
		// Setup for editing an existing item
		// Get the id from the GET fields
		$idparam = htmlspecialchars($_GET["id"]);
		
		// Lookup the item from the xml
		$xml = simplexml_load_file("xml/UBUCManual.xml");
		$itemNode = $xml->xpath(sprintf('//item[@id="%s"]', $idparam));
		
		// Fill the variables
		$itemName = $itemNode[0]['name'];
		$parentNode = $itemNode[0]->xpath('parent::*');
		$originalCollection = $parentNode[0]['name'];
		
		// Set a flag
		$newItem = false;
	}
	
	// Print the form elements
	printFormHead($itemName);
	printFormCollectionSelection($originalCollection);
	if($newItem){
		printNewItemAttributes($itemName);
	}else{
		printExistingItemAttributes($itemName, $itemNode[0]);
	}
	printFormHidenInputs($idparam);
	printFormEnd();
	
	// Print out the last error
	print_r(error_get_last());
}

/* Function for printing the form header */
function printFormHead($itemName){
	/* This function will print out both the HTML header block and the top line of the form
	 * Inputs:
	 * 		- $itemName - The name of the tem we are editing.
	 */
	
	// HTML Header elements
	print "<html>\n";
	print "<head>\n";
	print "<title>Edit &quot;". $itemName . "&quot;</title>\n";
	print "<link rel='stylesheet' type='text/css' href='css/UBUCManual.css' />\n";
	print "<script src='https://www.google.com/recaptcha/api.js'></script>\n";
	print "</head>\n";
	
	// Top of the form
	print "<body>";
	print "<form method='post' action='editItem.php'>\n";
	print "<h1>Edit &quot;". $itemName . "&quot;</h1>\n";
	print "<p>This form lets you edit an item or create a new one.</p>\n";
}

/* This function will print out the end bits of the form */
function printFormEnd(){
	/* This function prints out the end of the form, which
	 * includes the human verification, and the HTML page end.
	 */
	
	// Print out the verification
	print "<h2>Robot Check</h2>\n";
	print "<p>Prove that you are not a robot</p>\n";
	print "<div class='g-recaptcha' data-sitekey='6Lf7_Q8TAAAAAMlAwFUqeIM9tWxlG819RwvFOam8'></div>\n";
	print "<br/>\n";
	
	// Print the submit button
	print "<input type='submit' value='Confirm Changes' />\n";
	
	// Print out the html end
	print "</form>\n";
	print "</body>\n";
	print "</html>\n";
}

/* This function prints a page to confirm the input was OK */
function printConfirmationPage($redirectID, $itemName){
	echo "<html>\n";
	echo "<head>\n";
	echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
	if($redirectID != ""){
		$redirectid=$redirectID;
	}
	
	if (!isLocalhost()){
		echo '<meta http-equiv="refresh" content="1;url=http://ubuc.org/maintenance/Manual.php?id=',$redirectid,'">';
	}else{
		echo 'Redirect to id='. $redirectid;
	}
	echo "</head>\n";
	echo "<body>\n";
	echo "<h1>Thank you for your input</h1>\n";
	echo "<p>Item <b>" . $itemName . "</b> was edited!</p>\n";
	echo "<p>Redirecting in 1 second</p>\n";
	echo "</body>\n";
	echo "</html>";
}

/* Function for printing the form collection selector */
function printFormCollectionSelection($originalCollection){
	/* This will print a form element for selecting which collection to apply this
	 * procedure to */
	
	// Tell the user what they do
	print "<h2>Select Itemgroup</h2>\n";
	print "<p>The database has lots of itemgroups. These can be in 'collections'. You can place items anywhere.</p>\n";
	print "<p>Select which item group this item should be in.</p>\n";
	
	// Print the form element
	createCollectionSelection($originalCollection);
	
}

/* Function for printing out the form elements for a new item */
function printNewItemAttributes($itemName){
	/* This will print out all the things we think should be included for a new item */
	
	// Item name
	print "<h3>Name</h3>\n";
	print "<p>What is the name of the item?</p>\n";
	
	print "<textarea id='editName' name='editName' cols='70' rows='1' autocomplete='off'>\n";
	print $itemName;
	print "</textarea>\n";
	
	print "<h2>Other Properties</h2>\n";
	print "<p>Add other properties to your item to make it as descriptive as possible. Fill in as many as you can and leave ones you don't know blank.<br/>\n";
	print "There is even space for you to define your own.</p>\n";
	print "<p>Examples are:\n";
	print "<ul>\n";
	print "	<li><b>manufacturer</b> - The manufacturer that makes the item</li>\n";
	print "	<li><b>mass</b> - How much the item wheighs in kg</li>\n";
	print "	<li><b>material</b> - What the material is (steel? aluminium?)</li>\n";
	print "	<li><b>newvalue</b> - How much it cost to buy new</li>\n";
	print "	<li><b>serial</b> - The serial number</li>\n";
	print "	<li><b>year</b> - The year of manufacture</li>\n";
	print "	<li><b>stStage</b> - The type of second stage (for regs)</li>\n";
	print "	<li><b>model</b> - The manufcatures modle name/number</li>\n";
	print "	<li><b>size</b> - The size (small/large 10l/12l)</li>\n";
	print "</ul>\n";
	print "</p>\n";
	
	$parameters = array("manufacturer", "mass", "material", "newvalue", "serial", "year", "stStage", "model", "size");
	
	print "<table>\n";
	print "	<tr>\n";
	print "		<th><b>Property</b></th>\n";
	print "		<th><b>Value</b></th>\n";
	print "	</tr>\n";

	foreach($parameters as $parameter){
		print "<tr>\n";
		print "\t<td style='text-align:right'>" . $parameter . "</td>\n";
		print "\t<td><input type='Text' name='" . $parameter . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	for($i=1; $i<9; $i++){
		print "<tr>\n";
		print "\t<td><input type='Text' name='customParamName" . $i . "' autocomplete='off'/></td>\n";
		print "\t<td><input type='Text' name='customParamVal" . $i . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	print "</table>\n";
	
}

/* Function for printing out the form elements for a new item */
function printExistingItemAttributes($itemName, $itemNode){
	/* This will print out all the form elements to edit the items existing elements, and add new ones. */
	
	// Item name
	print "<h3>Name</h3>\n";
	print "<p>What is the name of the item?</p>\n";
	
	print "<textarea id='editName' name='editName' cols='70' rows='1' autocomplete='off'>\n";
	print $itemName;
	print "</textarea>\n";
	
	print "<h2>Other Properties</h2>\n";
	print "<p>Add other properties to your item to make it as descriptive as possible. Fill in as many as you can and leave ones you don't know blank.<br/>\n";
	print "There is even space for you to define your own.</p>\n";
	print "<p>Examples are:\n";
	print "<ul>\n";
	print "	<li><b>manufacturer</b> - The manufacturer that makes the item</li>\n";
	print "	<li><b>mass</b> - How much the item wheighs in kg</li>\n";
	print "	<li><b>material</b> - What the material is (steel? aluminium?)</li>\n";
	print "	<li><b>newvalue</b> - How much it cost to buy new</li>\n";
	print "	<li><b>serial</b> - The serial number</li>\n";
	print "	<li><b>year</b> - The year of manufacture</li>\n";
	print "	<li><b>stStage</b> - The type of second stage (for regs)</li>\n";
	print "	<li><b>model</b> - The manufcatures modle name/number</li>\n";
	print "	<li><b>size</b> - The size (small/large 10l/12l)</li>\n";
	print "</ul>\n";
	print "</p>\n";
	
	$parameters = array("manufacturer", "mass", "material", "newvalue", "serial", "year", "stStage", "model", "size");
	
	print "<table>\n";
	print "	<tr>\n";
	print "		<th><b>Property</b></th>\n";
	print "		<th><b>Value</b></th>\n";
	print "	</tr>\n";
	
	// Print out the recommended properties, if a value is already assigned then print it
	foreach($parameters as $parameter){
		// See if we can get a value for this parameter
		$propVal = $itemNode[$parameter];
		
		// Print the item
		print "<tr>\n";
		print "\t<td style='text-align:right'>" . $parameter . "</td>\n";
		print "\t<td><input type='Text' name='" . $parameter . "' autocomplete='off' value='" . $propVal . "'/></td>\n";
		print "</tr>\n";
	}
	
	// Print out any other existing properties
	$defaultParameters = array("id", "name", "manufacturer", "mass", "material", "newvalue", "serial", "year", "stStage", "model", "size");
	foreach($itemNode->attributes() as $name => $value){
		// Only print the item if it is not one we have already done
		if(!in_array($name, $defaultParameters)){
			print "<tr>\n";
			print "\t<td style='text-align:right'>" . $name . "</td>\n";
			print "\t<td><input type='Text' name='exist_" . $name . "' autocomplete='off' value='" . $value . "'/></td>\n";
			print "</tr>\n";
		}
	}
	
	// Print out custom properties
	for($i=1; $i<9; $i++){
		print "<tr>\n";
		print "\t<td><input type='Text' name='customParamName" . $i . "' autocomplete='off'/></td>\n";
		print "\t<td><input type='Text' name='customParamVal" . $i . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	print "</table>\n";
	
}

?>
