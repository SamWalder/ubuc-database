<?php
/*
 * This file allows the user to add or edit procedures in the database
 * Sam Walder - 2020
 */

// Bring in the required functions
require 'xmlFunctions.php';

// If the server has got to this page through the "POST" method it means the 
// "Submit" button on the form was clicked.
if ($_SERVER["REQUEST_METHOD"] == "POST"){
	
	// Assign the items that were posted to the form to variables
	$id   			= scrub($_POST["procID"]);
	$name 			= scrub($_POST["editName"]);
	$group 			= scrub($_POST["collection"]);
	$frequency 		= scrub($_POST["frequency"]);
	$importance     = scrub($_POST["Importance"]);
	$difficulty     = scrub($_POST["Difficulty"]);
	$reason     	= scrub($_POST["editReason"]);
	$effort       	= scrub($_POST["effort"]);
	$procedureText  = $_POST["EditContent"];
	
	// If they passed the robot test...
	if(isHuman() || isLocalhost()){
		
		// Load the xml manual file into the object
		$xml = simplexml_load_file("xml/UBUCManual.xml");
		
		// Select the proceedure node (if it exists)
		$procNode = $xml->xpath(sprintf('//procedure[@id="%s"]', $id));
		
		// If it is empty we need to make a new node in the correct group
		if (empty($procNode)){
			// Select the group using the ID provided
			$itemGroup = $xml->xpath(sprintf('//itemgroup[@id="%s"] | //collection[@id="%s"]', $group, $group));
			
			// Create the new child node in this group
			$procNode = $itemGroup[0]->addChild("procedure");
			
			// Add the ID
			$procNode->addAttribute("id", $id);
		} else {
			// We need to make sure it is in the correct itemgroup
			
			// Get the current parent node ID
			$parentNode = $xml->xpath(sprintf('//procedure[@id="%s"]/parent::*', $id));
			$parentID = $parentNode[0]['id'];
			
			// If they don't match we need to move our node
			if (strcmp($group, $parentID) !== 0 ){
				// Delete the old node
				$xml = deleteNode($xml, 'procedure', $id);
				
				// Select the group using the ID provided
				$itemGroup = $xml->xpath(sprintf('//itemgroup[@id="%s"] | //collection[@id="%s"]', $group, $group));
				
				// Create the new child node in this group
				$procNode = $itemGroup[0]->addChild("procedure");
				
				// Add the ID
				$procNode->addAttribute("id", $id);

			}
			
			$procNode = $procNode[0];
		}
		
		// Add the attributes to the node
		$procNode = editXMLAttribute($procNode, 'name', $name);
		$procNode = editXMLAttribute($procNode, 'frequency', $frequency);
		$procNode = editXMLAttribute($procNode, 'requirement', $importance);
		$procNode = editXMLAttribute($procNode, 'level', $difficulty);
		$procNode = editXMLAttribute($procNode, 'reason', $reason);
		$procNode = editXMLAttribute($procNode, 'effort', $effort);

		// Add the text
		$procNode[0] = htmlspecialchars($procedureText);
		
		// Print the DOM document to the screen
		//echo $xml->asXML();
		
		// Write the xml object to a file
		$xml->asXMl("xml/UBUCManual.xml");
		
		// Print anything that went wrong
		print_r(error_get_last());
		
		/* Create the confirmation that the input was recorded */
		printConfirmationPage($id, $name);
		
	}else{
		// Print a page to accuse them of being a robot
		printRobotPage();
	}
	
	// Stop the execution of this file. I don't think this is needed now as I am using the if else properly.
	exit();
	
}else{
	/* In the case we got to this form by any method other than "POST"
	 * then we render the edit procedure form */
	
	// If there is something in the get field then we are going to edit an existing procedure
	// Otherwise, we are making a new one
	if (empty($_GET)){
		// Setup for editing a new procedure
		$procName = "New Procedure";
		$origDescription = "Short description";
		$originalCollection = "None";
		$originalContent = "###Writing markdown\n\nPut your procedure content here. You can use basic markdown for this:\n\n###Heading\n\n####SubHeading\n\n*bold*\n\n[link text](http:/www.google.co.uk)";
		$origFrequnecy = 1;
		$origDifficulty = 'easy';
		$origEffort = 1;
		$origReason = "No reason";
		$origImportance = "recommended";
		$idparam = getNewID("proc001");
	}else{
		// Setup for editing an existing procedure
		// Get the id from the GET fields
		$idparam = htmlspecialchars($_GET["id"]);
		
		// Lookup the item from the xml
		$xml = simplexml_load_file("xml/UBUCManual.xml");
		$procedureNode = $xml->xpath(sprintf('//procedure[@id="%s"]', $idparam));
		
		// Fill the variables
		$procName = $procedureNode[0]['name'];
		$origDescription = $procedureNode[0]['description'];
		$parentNode = $procedureNode[0]->xpath('parent::*');
		$originalCollection = $parentNode[0]['name'];
		$originalContent = getProcContent($procedureNode[0]);
		$origFrequnecy = $procedureNode[0]['frequency'];
		$origDifficulty = $procedureNode[0]['level'];
		$origEffort = $procedureNode[0]['effort'];
		$origReason = $procedureNode[0]['reason'];
		$origImportance = $procedureNode[0]['requirement'];
	}
	
	// Print the form elements
	printFormHead($procName);
	printFormCollectionSelection($originalCollection);
	printFormOptions($procName, $origFrequnecy, $origDifficulty, $origEffort, $origReason, $origImportance);
	printFormContentEdit($originalContent);
	printFormHidenInputs($idparam);
	printFormEnd();
	
	// Print out the last error
	print_r(error_get_last());
}

/* Function for printing the form header */
function printFormHead($procName){
	/* This function will print out both the HTML header block and the top line of the form 
	 * Inputs:
	 * 		- $procName - The name of the proceedure we are editing.
	 */
	
	// HTML Header elements
	print "<html>\n";
	print "<head>\n";
	print "<title>Edit &quot;". $procName . "&quot; procedure</title>\n";
	print "<link rel='stylesheet' type='text/css' href='css/UBUCManual.css' />\n";
	print "<script src='https://www.google.com/recaptcha/api.js'></script>\n";
	print "</head>\n";
	
	// Top of the form
	print "<body>";
	print "<form method='post' action='editProcedure.php'>\n";
	print "<h1>Edit &quot;". $procName . "&quot; procedure</h1>\n";
	print "<p>This form lets you edit a procedure or create a new one.</p>\n";
}

/* Function for printing the form collection selector */
function printFormCollectionSelection($originalCollection){
	/* This will print a form element for selecting which collection to apply this 
	 * procedure to */
	
	// Tell the user what they do
	print "<h2>Select Itemgroup</h2>\n";
	print "<p>Each procedure will be applied to the items within the same item group that the procedure is in.</p>\n";
	print "<p>For example, if you have an item group of cylinders, you will want to have a 'hydrostatic test' within that collection.</p>\n";
	print "<p>Select which item group this procedure should apply to.</p>\n";
	
	// Print the form element
	createCollectionSelection($originalCollection);
	
}

/* Function for printing the controls for the other procedure properties */
function printFormOptions($origName, $origFrequnecy, $origDifficulty, $origEffort, $origReason, $origImportance){
	/* Prints out the form elements for controlling the other procedure properties */
	
	// Heading
	print "<h2>Edit The Procedure Options</h2>\n";
	print "<p>Edit other options for the procedure here</p>\n";
	
	// Procedure name
	print "<h3>Name</h3>\n";
	print "<p>What is the name of this procedure?</p>\n";
	
	print "<textarea id='editName' name='editName' cols='70' rows='1' autocomplete='off'>\n";
	print $origName;
	print "</textarea>\n";
	
	// Frequency
	print "<h3>Frequency</h3>\n";
	print "<p>How often (in months) does this procedure need to be compleated?</p>\n";
	
	print "<input type='Number' name='frequency' title='Frequency' value='" . $origFrequnecy . "' step='.01'/>\n";
	
	// Necessity
	print "<h3>Importance</h3>\n";
	print "<p>How important is it that this procedure is completed?</p>\n";
	print "<p>When a procedure goes out of date the items it applies to will get marked by the Daemon as:</p>\n";
	print "<ul>\n";
	print "<li>'broken' if the procedure is 'mandatory'</li>\n";
	print "<li>'damaged' if the procedure is 'required'</li>\n";
	print "<li>'niggle' if the procedure is 'recommended'</li>\n";
	print "</ul>\n";
	
	// Use a switch to create a selected attribute to inlude with each option in the list
	$selNA = '';
	$selRc = '';
	$selRq = '';
	$selMn = '';			
	switch ($origImportance){
		case 'NotAdvised':
			$selNA = ' selected="selected"';
			break;
		case 'recommended':
			$selRc = ' selected="selected"';
			break;
		case 'required':
			$selRq = ' selected="selected"';
			break;
		case 'mandatory':
			$selMn = ' selected="selected"';
			break;
	}
	
	print "<label for='Importance'>Importance:</label>\n";
	print "<select id='Importance' name='Importance' autocomplete='off'>\n";
		print "<option value='NotAdvised'" . $selNA . ">\n";
		print "NotAdvised\n";
		print "</option>\n";
		print "<option value='recommended'" . $selRc . ">\n";
		print "recommended\n";
		print "</option>\n";
		print "<option value='required'" . $selRq . ">\n";
		print "required\n";
		print "</option>\n";
		print "<option value='mandatory'" . $selMn . ">\n";
		print "mandatory\n";
		print "</option>\n";
	print "</select>\n";
	
	// Difficulty
	print "<h3>Difficulty</h3>\n";
	print "<p>How difficult is this? Mark as 'commercial' if it must be done by a professional.</p>\n";
	
	// Use a switch to create a selected attribute to inlude with each option in the list
	$selE = '';
	$selH = '';
	$selC = '';
	switch ($origDifficulty){
		case 'easy':
			$selE = ' selected="selected"';
			break;
		case 'hard':
			$selH = ' selected="selected"';
			break;
		case 'commercial':
			$selC = ' selected="selected"';
			break;
	}
	
	print "<label for='Difficulty'>Difficulty:</label>\n";
	print "<select id='Difficulty' name='Difficulty' autocomplete='off'>\n";
		print "<option value='easy'" . $selE . ">\n";
		print "easy\n";
		print "</option>\n";
		print "<option value='hard'" . $selH . ">\n";
		print "hard\n";
		print "</option>\n";
		print "<option value='commercial'" . $selC . ">\n";
		print "commercial\n";
		print "</option>\n";
	print "</select>\n";
	
	// Reason for doing it
	print "<h3>Reason</h3>\n";
	print "<p>Why do we do this procedure?</p>\n";
	
	print "<textarea id='editReason' name='editReason' cols='70' rows='4' autocomplete='off'>\n";
	print $origReason;
	print "</textarea>\n";
	
	// Effort to do it
	print "<h3>Effort</h3>\n";
	print "<p>How much effort (in hours) does it take to compleate this procedure?</p>\n";
	
	print "<input type='Number' name='effort' title='Effort' value='" . $origEffort . "' step='.01'/>\n";
	
}

/* Function for printing the form content edit box */
function printFormContentEdit($originalContent){
	/* This will print out the content of the procedure and allow the user to edit it */
	
	// Tell the user what they do
	print "<h2>Edit The Procedure Text</h2>\n";
	print "<p>Edit the text describing the procedure here. You can use <a href='https://guides.github.com/features/mastering-markdown/'>markdown</a> for this.</p>\n";
	print "<p>You can also reference other procedures, parts, references, consumables etc using their reference ID.\n";
	print "Example: Reference the Engine Operation procedure using: [[EngineOperation]] \n";
	print "</p>\n";
	
	// Create the form element
	print "<textarea id='EditContent' name='EditContent' cols='100' rows='30' autocomplete='off'>\n";
	print $originalContent;
	print "</textarea>\n";
}

/* This function will print out the end bits of the form */
function printFormEnd(){
	/* This function prints out the end of the form, which 
	 * includes the human verification, and the HTML page end.
	 */
	
	// Print out the verification
	print "<h2>Robot Check</h2>\n";
	print "<p>Prove that you are not a robot</p>\n";
	print "<div class='g-recaptcha' data-sitekey='6Lf7_Q8TAAAAAMlAwFUqeIM9tWxlG819RwvFOam8'></div>\n";
	print "<br/>\n";
	
	// Print the submit button
	print "<input type='submit' value='Confirm Changes' />\n";
	
	// Print out the html end
	print "</form>\n";
	print "</body>\n";
	print "</html>\n";
}

/* This function will return a string with the original proceedure content */
function getProcContent($procedureNode){
	/* Parse a procedure node to this function and it will return the content as a string.
	 * My intention is to move to using markdown for the procedure text, but I
	 * don't want to go through and manualy transcribe all the procedures to this.
	 * Instead, this function will either return the existing markdown, or create it from the 
	 * original format.
	 */
	
	// Return text blank
	$returnText = '';
	
	// Check if there is already markdown - assume that it is not if it contains a 'step' node.
	if (!isset($procedureNode->step)){
		$returnText = htmlspecialchars_decode($procedureNode);
	} else{
		// If not, then we need to create it
		for ($iStep=0; $iStep<count($procedureNode->step); $iStep++){
			// Step name
			$returnText = $returnText .  "### " . $procedureNode->step[$iStep]["name"] . "\n";
			// Step text
			$stepText = $procedureNode->step[$iStep]->asXML();
			$firstTagClose = strpos($stepText, ">");
			$stepText = substr($stepText, $firstTagClose + 1, -7);
			$stepText = convertRefsToMDown($stepText);
			$returnText = $returnText . $stepText . "\n\n";
		}
	}
	
	// Return the text
	return $returnText;
}

/* This function prints a page to confirm the input was OK */
function printConfirmationPage($redirectID, $procName){
	echo "<html>";
	echo "<head>";
	echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
	if($redirectID != ""){
		$redirectid=$redirectID;
	}
	
	if (!isLocalhost()){
		echo '<meta http-equiv="refresh" content="1;url=http://ubuc.org/maintenance/Manual.php?id=',$redirectid,'">';
	}
	echo "</head>";
	echo "<body>";
	echo "<h1>Thank you for your input</h1>";
	echo "<p>Procedure <b>" . $procName . "</b> was edited!</p>";
	echo "<p>Redirecting in 1 second</p>";
	echo "</body>";
	echo "</html>";
}

/* This function converts reference nodes to markdown */
function convertRefsToMDown($inText){
	/* This function will take a text string and convert any ref tags 
	 * to markdown. ie. <refid = "West01"> converts to [[West01]]
	 */
	
	// Use a regex to match the tags
	preg_match_all('/\<[A-Za-z0-9]*\sid="[A-Za-z0-9\s\"=]*"\/\>/m', $inText, $matches, PREG_OFFSET_CAPTURE);
	
	// Go through each match and replace the text
	foreach ($matches[0] as $matchItem){
		// Get the id string
		preg_match('/id="[A-Za-z0-9\s=]*"/m', $matchItem[0], $idMatch);
		$id = substr($idMatch[0], 4, -1);
		
		// Replace the tag with the markdown
		$newString = "[[" . $id . "]]";
		//print $matchItem[0] . "\n";
		//print $newString . "\n";
		$inText = str_replace($matchItem[0], $newString, $inText);
	}
	
	// Return the result
	return $inText;
	
}

?>
