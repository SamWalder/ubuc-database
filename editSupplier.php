<?php
/*
 * This file allows the user to add new supplier to the database or edit existing ones
 * Sam Walder - 2021
 */

// Bring in the required functions
require 'xmlFunctions.php';

// If the server has got to this page through the "POST" method it means the "Submit" button on the form was clicked.
if ($_SERVER["REQUEST_METHOD"] == "POST"){

	// Assign the items that were posted to the form to variables
	$id   = scrub($_POST["procID"]);		// The item ID we are going to use
	$name = scrub($_POST["editName"]);			// The name of the supplier

	// Properties
	$email 		= scrub($_POST["email"]);
	$tel        = scrub($_POST["tel"]);
	$uri     	= scrub($_POST["uri"]);
	$postcode	= scrub($_POST["postcode"]);

	// If they passed the robot test...
	if(isHuman() || isLocalhost()){

		// Load the xml manual file into the object
		$xml = simplexml_load_file("xml/suppliers.xml");
		
		// Select the item node (if it exists)
		$supNode = $xml->xpath(sprintf('//supplier[@id="%s"]', $id));
		
		// If it is empty we need to make a new node
		if (empty($supNode)){
			
			// Get a new unique id
			$id = getNewSupplierID();
			
			// Create the new child node
			$supNode = $xml->addChild("supplier");
			
			// Add the ID
			$supNode->addAttribute("id", $id);
			
			$supNode = $supNode[0];
		} else {
			$supNode = $supNode[0];
		}
		
		// Add or update each of the default parameters
		if (!empty($name)){
			$supNode = editXMLAttribute($supNode, 'name', $name);
		}
		if (!empty($email)){
			$supNode = editXMLAttribute($supNode, 'email', $email);
		}
		if (!empty($tel)){
			$supNode = editXMLAttribute($supNode, 'tel', $tel);
		}
		if (!empty($uri)){
			$supNode = editXMLAttribute($supNode, 'uri', $uri);
		}
		if (!empty($postcode)){
			$supNode = editXMLAttribute($supNode, 'postcode', $postcode);
		}
		
		// Now add any custom properties that were added
		for ($paramNo=1; $paramNo<4; $paramNo++){
			$thisProp = scrub($_POST["customParamName" . $paramNo]);
			$thisVal = scrub($_POST["customParamVal" . $paramNo]);
			
			// Edit the value if it is not empty
			if (!empty($thisProp) && !empty($thisVal)){
				$supNode = editXMLAttribute($supNode, $thisProp, $thisVal);
			}
		}
		
		// Now see if there were any other keys passed
		// Loop through everything that was posted
		foreach($_POST as $postName => $postVal){
			// If the post name starts with 'exist_' it is a custom param
			if(strcmp(substr($postName, 0, 6), "exist_") == 0){
				if(empty($postVal)){
					// If the value is empty we will remove the attribute
					$supNode = removeXMLAttribute($supNode, scrub(substr($postName, 6)));
				}else{
					// If the value is not empty, we will edit the attribute
					$supNode = editXMLAttribute($supNode, scrub(substr($postName, 6)), scrub($postVal));
				}
			}
		}

		// Write the xml object to a file
		$xml->asXMl("xml/suppliers.xml");
		
		// Print anything that went wrong
		print_r(error_get_last());
		
		/* Create the confirmation that the input was recorded */
		printConfirmationPage($id, $name);
		
	}else{
		// Print a page to accuse them of being a robot
		printRobotPage();
	}

	// Stop the execution of this file. I don't think this is needed now as I am using the if else properly.
	exit();
 
}else{
	// In the case we got to this form by any method other than "POST" then we render the edit item form

	// If there is something in the get field then we are going to edit an existing item
	// Otherwise, we are making a new one
	if (empty($_GET)){
		// Setup for editing a new item
		$supName = "New Supplier";
		$idparam = 'null';
		$supNode = "";
		
		// Set a flag
		$newSup = true;
	}else{
		// Setup for editing an existing supplier
		// Get the id from the GET fields
		$idparam = htmlspecialchars($_GET["id"]);
		
		// Lookup the item from the xml
		$xml = simplexml_load_file("xml/suppliers.xml");
		$supNode = $xml->xpath(sprintf('//supplier[@id="%s"]', $idparam));
		
		// Fill the variables
		$supName = $supNode[0]['name'];
		
		// Set a flag
		$newSup = false;
	}
	
	// Print the form elements
	printFormHead($supName);
	if($newSup){
		printNewSupplierAttributes($supName);
	}else{
		printExistingSupplierAttributes($supName, $supNode[0]);
	}
	printFormHidenInputs($idparam);
	printFormEnd();
	
	// Print out the last error
	print_r(error_get_last());
}

/* Function for printing the form header */
function printFormHead($supName){
	/* This function will print out both the HTML header block and the top line of the form
	 * Inputs:
	 * 		- $supName - The name of the supplier we are editing.
	 */
	
	// HTML Header elements
	print "<html>\n";
	print "<head>\n";
	print "<title>Edit &quot;". $supName . "&quot;</title>\n";
	print "<link rel='stylesheet' type='text/css' href='css/UBUCManual.css' />\n";
	print "<script src='https://www.google.com/recaptcha/api.js'></script>\n";
	print "</head>\n";
	
	// Top of the form
	print "<body>";
	print "<form method='post' action='editSupplier.php'>\n";
	print "<h1>Edit &quot;". $supName . "&quot;</h1>\n";
	print "<p>This form lets you edit a supplier or create a new one.</p>\n";
}

/* This function will print out the end bits of the form */
function printFormEnd(){
	/* This function prints out the end of the form, which
	 * includes the human verification, and the HTML page end.
	 */
	
	// Print out the verification
	print "<h2>Robot Check</h2>\n";
	print "<p>Prove that you are not a robot</p>\n";
	print "<div class='g-recaptcha' data-sitekey='6Lf7_Q8TAAAAAMlAwFUqeIM9tWxlG819RwvFOam8'></div>\n";
	print "<br/>\n";
	
	// Print the submit button
	print "<input type='submit' value='Confirm Changes' />\n";
	
	// Print out the html end
	print "</form>\n";
	print "</body>\n";
	print "</html>\n";
}

/* This function prints a page to confirm the input was OK */
function printConfirmationPage($redirectID, $supName){
	echo "<html>\n";
	echo "<head>\n";
	echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
	if($redirectID != ""){
		$redirectid=$redirectID;
	}
	
	if (!isLocalhost()){
		echo '<meta http-equiv="refresh" content="1;url=http://ubuc.org/maintenance/Manual.php?id=',$redirectid,'">';
	}else{
		echo 'Redirect to id='. $redirectid;
	}
	echo "</head>\n";
	echo "<body>\n";
	echo "<h1>Thank you for your input</h1>\n";
	echo "<p>Supplier <b>" . $supName . "</b> was edited!</p>\n";
	echo "<p>Redirecting in 1 second</p>\n";
	echo "</body>\n";
	echo "</html>";
}

/* Function for printing out the form elements for a new Supplier */
function printNewSupplierAttributes($supName){
	/* This will print out all the things we think should be included for a new supplier */
	
	// Item name
	print "<h2>Name</h2>\n";
	print "<p>What is the name of the supplier?</p>\n";
	
	print "<textarea id='editName' name='editName' cols='70' rows='1' autocomplete='off'>\n";
	print $supName;
	print "</textarea>\n";
	
	print "<h2>Other Properties</h2>\n";
	print "<p>Add other properties to your supplier to make it as descriptive as possible. Fill in as many as you can and leave ones you don't know blank.<br/>\n";
	print "There is even space for you to define your own.</p>\n";
	print "<p>Examples are:\n";
	print "<ul>\n";
	print "	<li><b>email</b> - Their email address</li>\n";
	print "	<li><b>tel</b> - Their telephone number</li>\n";
	print "	<li><b>uri</b> - Their website</li>\n";
	print "	<li><b>postcode</b> - Their postcode</li>\n";
	print "</ul>\n";
	print "</p>\n";
	
	$parameters = array("email", "tel", "uri", "postcode");
	
	print "<table>\n";
	print "	<tr>\n";
	print "		<th><b>Property</b></th>\n";
	print "		<th><b>Value</b></th>\n";
	print "	</tr>\n";

	foreach($parameters as $parameter){
		print "<tr>\n";
		print "\t<td style='text-align:right'>" . $parameter . "</td>\n";
		print "\t<td><input type='Text' name='" . $parameter . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	for($i=1; $i<4; $i++){
		print "<tr>\n";
		print "\t<td><input type='Text' name='customParamName" . $i . "' autocomplete='off'/></td>\n";
		print "\t<td><input type='Text' name='customParamVal" . $i . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	print "</table>\n";
	
}

/* Function for printing out the form elements for a new supplier */
function printExistingSupplierAttributes($supName, $supNode){
	/* This will print out all the form elements to edit the suppliers existing elements, and add new ones. */
	
	// Item name
	print "<h2>Name</h2>\n";
	print "<p>What is the name of the supplier?</p>\n";
	
	print "<textarea id='editName' name='editName' cols='70' rows='1' autocomplete='off'>\n";
	print $supName;
	print "</textarea>\n";
	
	print "<h2>Other Properties</h2>\n";
	print "<p>Add other properties to your supplier to make it as descriptive as possible. Fill in as many as you can and leave ones you don't know blank.<br/>\n";
	print "There is even space for you to define your own.</p>\n";
	print "<p>Examples are:\n";
	print "<ul>\n";
	print "	<li><b>email</b> - Their email address</li>\n";
	print "	<li><b>tel</b> - Their telephone number</li>\n";
	print "	<li><b>uri</b> - Their website</li>\n";
	print "	<li><b>postcode</b> - Their postcode</li>\n";
	print "</ul>\n";
	print "</p>\n";
	
	$parameters = array("email", "tel", "uri", "postcode");
	
	print "<table>\n";
	print "	<tr>\n";
	print "		<th><b>Property</b></th>\n";
	print "		<th><b>Value</b></th>\n";
	print "	</tr>\n";
	
	// Print out the recommended properties, if a value is already assigned then print it
	foreach($parameters as $parameter){
		// See if we can get a value for this parameter
		$propVal = $supNode[$parameter];
		
		// Print the item
		print "<tr>\n";
		print "\t<td style='text-align:right'>" . $parameter . "</td>\n";
		print "\t<td><input type='Text' name='" . $parameter . "' autocomplete='off' value='" . $propVal . "'/></td>\n";
		print "</tr>\n";
	}
	
	// Print out any other existing properties
	$defaultParameters = array("id", "name", "email", "tel", "uri", "postcode");
	foreach($supNode->attributes() as $name => $value){
		// Only print the item if it is not one we have already done
		if(!in_array($name, $defaultParameters)){
			print "<tr>\n";
			print "\t<td style='text-align:right'>" . $name . "</td>\n";
			print "\t<td><input type='Text' name='exist_" . $name . "' autocomplete='off' value='" . $value . "'/></td>\n";
			print "</tr>\n";
		}
	}
	
	// Print out custom properties
	for($i=1; $i<4; $i++){
		print "<tr>\n";
		print "\t<td><input type='Text' name='customParamName" . $i . "' autocomplete='off'/></td>\n";
		print "\t<td><input type='Text' name='customParamVal" . $i . "' autocomplete='off'/></td>\n";
		print "</tr>\n";
	}
	
	print "</table>\n";
	
}

?>
