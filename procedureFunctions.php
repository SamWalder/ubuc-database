<?php
/* This file contains all the functions to deal with procedure pages.
 * Sam Walder - 2020
 */

// Pull in the Parsdown class
require_once('Parsedown.php');

function idIsProcedure($id){
	/* This function will check the manual XML and wil return true if the id 
	 * parsed is one that identifies a procedure node.
	 */
	
	// Load the xml manual file into an object
	$xml = simplexml_load_file("xml/UBUCManual.xml");
	
	// Use XPath to select a node which mataches the ID and the type
	$selected = $xml->xpath(sprintf('//procedure[@id="%s"]', $id));
	
	// Check if we got a valid procedure
	if (!empty($selected)){
		return true;
	}else{
		return false;
	}
}

function displayProcedure($id){
	/* This function will print out the procedure page for the given ID */
	
	// Load the manual as a DOM document
	$doc = new DOMDocument();
	$doc->load('xml/UBUCManual.xml');
	
	// Load the xslt as a DOM document
	$XSL = new DOMDocument();
	$XSL->load( 'xslt/procedure.xslt', LIBXML_NOCDATA);
	
	// Import the xlst into the xslt processor
	$xslt = new XSLTProcessor();
	$xslt->importStylesheet( $XSL );
	
	// Setup the parameters in the xslt document
	$xslt->setParameter('', 'id', $id);
	$xslt->setParameter('', 'CurrentDate', date("Y-m-d"));
	
	// Enable php functions in the xslt
	$xslt->registerPHPFunctions();
	
	// Print the xslt
	print $xslt->transformToXML( $doc );
	
}


function parseMarkdown($inText){
	/* This function will take the markdown from the procedure and present it as HTML.
	 * It will also convert references to site items into full formed links.
	 */
	
	// Replace ref tags with runn links
	$inText = expandSiteRefs($inText);
	
	// Run the text through the markdown parser
	$Parsedown = new Parsedown();
	return $Parsedown->text($inText);

}

function expandSiteRefs($inText){
	/* This function will look for site IDs in double square brackets and convert them to markdown links */
	
	// Use a regex to match the tags
	preg_match_all('/\[\[[A-Za-z0-9]*\]\]/m', $inText, $matches, PREG_OFFSET_CAPTURE);
	
	// Load the xml manual file into an object
	$xml = simplexml_load_file("xml/UBUCManual.xml");
	
	// Go through each match and replace the text
	foreach ($matches[0] as $matchItem){
		// Get the id string
		preg_match('/id="[A-Za-z0-9\s=]*"/m', $matchItem[0], $idMatch);
		$id = substr($matchItem[0], 2, -2);
		
		// Look up the node
		$itemNode = $xml->xpath(sprintf('//parts//partbit[@id="%s"] | //parts//part[@id="%s"] | //tools//tool[@id="%s"] | //consumables//consumable[@id="%s"] | //references//ref[@id="%s"]', $id, $id, $id, $id, $id));
		
		// Get a readable name if we can
		$name = $itemNode[0]['name'];
		if(!empty($name)){
			$niceText = $name;
		} else {
			$niceText = $id;
		}
		
		// Replace the tag with the link
		$newString = "[" . $niceText . "](http://ubuc.org/maintenance/Manual.php?id=" . $id . ")";
		$inText = str_replace($matchItem[0], $newString, $inText);
	}
	
	// Return the result
	return $inText;
}


/* This function makes a list of requried items */
function listRequiredItems($inText){
	/* This function will take the provided markdown text with the sqare bracket local refs and make a list of the required items.
	 * The output is a set of HTML table rows with the folliwng column info:
	 * 		- Item 					- The name of the item as a link to the items manual page
	 * 		- Attributes 			- A list of all the attributes other than 'id', 'name', 'image', or 'store'.
	 * 		- In Stores?			- the value of the 'store' attribute
	 */
	
	// Use a regex to match the tags
	preg_match_all('/\[\[[A-Za-z0-9]*\]\]/m', $inText, $matches, PREG_OFFSET_CAPTURE);
	
	// Load the xml manual file into an object
	$xml = simplexml_load_file("xml/UBUCManual.xml");
	
	// Setup the output text
	$outputText = '';
	
	// Go through each match and lookup the id
	foreach ($matches[0] as $matchItem){
		// Get the id string
		preg_match('/id="[A-Za-z0-9\s=]*"/m', $matchItem[0], $idMatch);
		$id = substr($matchItem[0], 2, -2);
		
		// Select the item with Xpath. It must be a child of a 'parts', 'tools', or 'consumables' node.
		$Itemnode = $xml->xpath(sprintf('//parts//partbit[@id="%s"] | //parts//part[@id="%s"] | //tools//tool[@id="%s"] | //consumables//consumable[@id="%s"]', $id, $id, $id, $id));
		
		// If we matched a node then it is one we want in this list
		if (!empty($Itemnode)){
			
			// Print out its information
			$outputText = $outputText . "<tr>\n";
			$outputText = $outputText . "<td><a href='http://ubuc.org/maintenance/Manual.php?id=" . $id . "' onmouseover='nhpup.popup($(&#39;#" . $id . " &#39;).html(), {&#39;width&#39;:200})'>" . $Itemnode[0]['name'] . "</a></td>\n";
			$outputText = $outputText . "<td>\n";
			foreach($Itemnode[0]->attributes() as $att => $val){
				if (strcmp($att, 'name') && strcmp($att, 'id') && strcmp($att, 'store')){
					$outputText = $outputText . $att . " = " . $val . "</br>\n";
				}
			}
			$outputText = $outputText . "</td>\n";
			$outputText = $outputText . "<td>" . $Itemnode[0]['store'] . "</td>\n";
			$outputText = $outputText . "</tr>\n";
		}
		
	}
	
	// Return the result text
	return $outputText;
	
	
}


?>