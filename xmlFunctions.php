<?php
/* This is a file full of useful functions which I wish to share between files 
 * Sam Walder - 2020
 */

/* Define a function for scrubbing text */
function scrub($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

/* Function for checking that an item id is unique */
function IDIsUnique($IDToCheck, $XML){
	/* This function will return true if the passed ID does not appear
	 * in any of the items in the input simpleXML object */
	
	// Try selecting the item ID using Xpath
	$existingItem = $XML->xpath(sprintf('//*[@id="%s"]', $IDToCheck));
	
	// See if anything was returned
	if (empty($existingItem)){
		// Item does not exist
		return true;
	} else {
		// Item already exists
		return false;
	}
}

/* Function for incrementing an id number */
function incrementID($lastID){
	/* This function will take the passed ID and increment the number at the end of the string */
	
	// Get just the integers from the ID field
	$lastIDIdx = intval(filter_var($lastID, FILTER_SANITIZE_NUMBER_INT));
	
	// Get just the characters from the ID field
	$lastIDName = '';
	preg_match('/^[a-zA-Z]*/', $lastID, $lastIDName);
	
	// Put together the new ID
	$newID = $lastIDName[0] . sprintf("%'.03d", $lastIDIdx+1);
	
	// Return the results
	return $newID;
}

/* Function for verifying the user is not a robot */
function isHuman(){
	/* This function will verify the Google site verify
	 * to check that the user is human.
	 * It is dependant on the inclusion of the correct scripts in the page header and the inclusion of the captch box.
	 */
	
	// Captcha info
	$gresponse=$_POST["g-recaptcha-response"];
	
	// Check whether we have a valid user
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array('secret' => '6Lf7_Q8TAAAAAOkqdoU4EYSzNH3GcMyaNsHqyllw', 'response' => $gresponse);
	
	// use key 'http' even if you send the request to https://...
	$options = array(
			'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data),
			),
	);
	
	// Get the information from the Google site verify service
	$context = stream_context_create($options);
	$gverify = file_get_contents($url, false, $context);
	
	$decodedgverify = json_decode($gverify);
	
	# Output information about allow_url_fopen:
	if (ini_get('allow_url_fopen') == 1) {
		# Do nothing
	} else {
		echo '<p style="color: #A00;">fopen is not allowed on this host. This form will not work.</p>';
	}
	
	// If they passed the robot test...
	if($decodedgverify->{"success"} == true){
		return true;
	} else {
		return false;
	}
}

/* Define a function which creates a new unique ID */
function getNewID($seed){
	/* This will create a new ID for a database item. If provided, it will use the input seed as the route of the ID name */
	
	// Make sure we have a seed
	if(empty($seed)){
		// If the seed is empty we fill it with something naff
		$seed = "id001";
	}
	
	// Load the xml manual file into an object
	$xml = simplexml_load_file("xml/UBUCManual.xml");
	
	// Make it unique
	$newID = $seed;
	while (!IDIsUnique($newID, $xml)){
		$newID = incrementID($newID);
	}
	
	// Return
	return $newID;
}

/* Define a function which creates a new unique ID for a supplier */
function getNewSupplierID(){
	/* This will create a new ID for a database supplier.*/
	
	$seed = "sp001";
	
	// Load the xml suppliers file into an object
	$xml = simplexml_load_file("xml/suppliers.xml");
	
	// Make it unique
	$newID = $seed;
	while (!IDIsUnique($newID, $xml)){
		$newID = incrementID($newID);
	}
	
	// Return
	return $newID;
}

/* Define a function for checking if we are on localhost */
function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
	/* Returns true if we are running on localhost */
    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

/* Define a function for editing a node attribute */
function editXMLAttribute($xmlNode, $attributeName, $newValue){
	/* This function will edit the value of an attribute node.
	 * If the node does not exist, it will create it.
	 */
	
	if (isset($xmlNode[$attributeName])){
		$xmlNode->attributes()->$attributeName = $newValue;
	} else {
		$xmlNode->addAttribute($attributeName, $newValue);
	}
	
	return $xmlNode;
}

/* Define a function for removing a node attribute */
function removeXMLAttribute($xmlNode, $attributeName){
	/* This function will remove an attribute node.
	 */
	
	if (isset($xmlNode[$attributeName])){
		unset($xmlNode[$attributeName]);
	} else {
		// It does not exist
	}
	
	return $xmlNode;
}

/* Define a function for printing a page when they fail the robot test */
function printRobotPage(){
	echo "<html>";
	echo "<head>";
	echo '<link rel="stylesheet" type="text/css" href="css/UBUCManual.css">';
	echo "</head>";
	echo "<body><h1>We think you might be a robot!</h1>";
	echo '<img src="Resources/webbot.png"/>';
	echo "<h2>If you are a human</h2>";
	echo "Your new item has been ignored as its assumed to be spam!<br/>";
	echo "Go back to the add item page and remember to click the box ";
	echo "indicating you are not a robot</body></html>"	;
}

/* Function to delete a node from an xml object */
function deleteNode($xml, $type, $id){
	/* This function will compleatly remove a node from thethe input xml object
	 * The function will return the object without the node.
	 * You must specify the node type as a string
	 */
	
	// Select the node to be removed
	$nodeToRemove = $xml->xpath(sprintf('//%s[@id="%s"]', $type, $id));
	
	// Remove the node using the DOM
	$dom = dom_import_simplexml($nodeToRemove[0]);
	$dom->parentNode->removeChild($dom);
	
	// Return the result
	return $xml;
	
}

/* Function for creting a string of the hierarchy to the current node */
function createHierarchyString($node){
	/* This function will create a string of the parents the the passed node.
	 * Node needs to be a simple xml object.
	 */
	
	// Start the out string with the name of the current node
	$outString = $node['name'];
	
	// Work up each parent and add it to the string
	$parent = $node->xpath('parent::*');
	while (!empty($parent)){
		// Add to  the string
		$outString = $parent[0]['name'] . " -> " . $outString;
		
		// Get the next parent
		$parent = $parent[0]->xpath('parent::*');
	}
	
	// Return the string
	return $outString;
	
}

/* Function for printing out a form element which allows the user to select the different itemgroups and collections */
function createCollectionSelection($originalCollection){
	// Load the xml manual file into an object
	$xml = simplexml_load_file("xml/UBUCManual.xml");
	
	// Select all the collection nodes
	$collections = $xml->xpath('//collection');
	
	// Create the form element, looping through all possible itemgroups
	print "<label for='collection'>Itemgroup/Collection:</label>\n";
	print "<select id='collection' name='collection' autocomplete='off'>\n";
	for ($col = 0; $col < count($collections); $col++){
		
		// Print out the option for selecting the collection
		print "<option value='" . $collections[$col]['id'] . "'" ;
		if(strcmp($originalCollection, $collections[$col]['name']) == 0){print " selected='selected'";}
		print ">\n";
		print "Collection: " . substr(createHierarchyString($collections[$col]), 12) . "\n";
		print "</option>\n";
		
		// Select all the itemgroup nodes in this collection
		$itemgroups = $collections[$col]->xpath('child::itemgroup');
		
		for ($group = 0; $group < count($itemgroups); $group++){
			print "<option value='" . $itemgroups[$group]['id'] . "'" ;
			if(strcmp($originalCollection, $itemgroups[$group]['name']) == 0){print " selected='selected'";}
			print ">\n";
			print substr(createHierarchyString($itemgroups[$group]), 12) . "\n";
			print "</option>\n";
		}
	}
	print "</select>\n";
}

/* Function for printing hidden form inputs */
function printFormHidenInputs($ID){
	/* This will print the hidden form elements */
	
	// Print a hidden input so that we send the ID to back to the server
	print "<input type='hidden' id='procID' name='procID' value='" . $ID . "'>\n";
	
}

?>