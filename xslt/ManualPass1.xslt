<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" indent="yes" />
   <!-- Create a lookup table for items and their state -->
   <xsl:param name="id"/>

 <xsl:template match="@*|node()">
   <xsl:copy>
     <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 </xsl:template>

<xsl:template match="manual">
   <xsl:copy>
     <xsl:apply-templates select="node()"/>
   </xsl:copy>
</xsl:template>

<xsl:template match="item">
   <xsl:variable name="thisitem"><xsl:value-of select="@id"/></xsl:variable>
     <xsl:variable name="opstatus">
	 <xsl:variable name="status">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem 
	 and (@commenttype='damaged' or @commenttype='broken' or @commenttype='niggle' or @commenttype='working'
	 or @commenttype='missing' or @commenttype='decomissioned')]">
	  <xsl:sort select="@date" order="descending"/>
  <xsl:if test="position() = 1">
              <xsl:value-of select="@commenttype" />
	      </xsl:if>
     </xsl:for-each>
     </xsl:variable>
	 <xsl:if test="$status=''">working</xsl:if>
	 <xsl:if test="$status!=''">
         <xsl:value-of select="$status"/>
	 </xsl:if>
     </xsl:variable>
   <xsl:copy>

   <!-- Add an operational status of this item -->
     <xsl:attribute name="status">
	   <xsl:value-of select="$opstatus"/>
	 </xsl:attribute>

     <!-- If the current status is not working then add Repair Cost -->

	 <xsl:if test="$opstatus='niggle' or $opstatus='damaged' or $opstatus='broken'">
 
     <xsl:variable name="repaircost">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem 
	 and (@commenttype='damaged' or @commenttype='broken' or @commenttype='niggle')][last()]">
         <xsl:value-of select="@price" />
     </xsl:for-each>
	 </xsl:variable>
 
     <xsl:if test="string-length($repaircost)>0">
     <xsl:attribute name="repaircost"><xsl:value-of select="$repaircost"/></xsl:attribute>
	 </xsl:if>
	 </xsl:if>
     

	 <xsl:variable name="currentvalue">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem 
	 and (@commenttype='valuation')][last()]">
         <xsl:value-of select="@price" />
     </xsl:for-each>
	 </xsl:variable>
	 <xsl:if test="string-length($currentvalue)">
	 <xsl:attribute name="currentvalue"><xsl:value-of select="$currentvalue"/></xsl:attribute>
	 </xsl:if>

	 <xsl:variable name="newvalue">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem 
	 and (@commenttype='quote')][last()]">
         <xsl:value-of select="@price" />
     </xsl:for-each>
	 </xsl:variable>
	 <xsl:if test="string-length($newvalue)>0">
	 	 <xsl:attribute name="newvalue"><xsl:value-of select="$newvalue"/></xsl:attribute>
     </xsl:if>

	 <xsl:variable name="image">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem 
	 and (@commenttype='image')][last()]">
         <xsl:value-of select="@uri" />
     </xsl:for-each>
	 </xsl:variable>
	 <xsl:if test="string-length($image)>0">
	 	 <xsl:attribute name="image"><xsl:value-of select="$image"/></xsl:attribute>
     </xsl:if>
    

     <xsl:apply-templates select="@*"/>
	 <xsl:apply-templates select="node()"/>
	 <!-- xsl:apply-templates select="." mode="intable">
                              <xsl:with-param name="thisitem" select="$thitem">
                              </xsl:with-param>
                           </xsl:apply-templates -->


   </xsl:copy>
</xsl:template>

<xsl:template match="procedure">
   <xsl:variable name="thisproc"><xsl:value-of select="@id"/></xsl:variable> 
   <xsl:copy>
	 <xsl:variable name="estimate">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@procedure=$thisproc 
	 and (@commenttype='estimate')][last()]">
         <xsl:value-of select="@price" />
         </xsl:for-each>
	     </xsl:variable>
	    <xsl:if test="string-length($estimate)">
	 <xsl:attribute name="BudgetEstimate"><xsl:value-of select="$estimate"/></xsl:attribute>
<!-- Create a list of items to which this procedure applies and the list date-->
</xsl:if>

     <xsl:apply-templates select="@*|node()"/>

	 <itemlist>
      <xsl:for-each select="../item|../itemgroup/item">
         <xsl:variable name="thisvar"><xsl:value-of select="@id"/></xsl:variable>

         <xsl:variable name="lastdone">
            <xsl:for-each select="document('../xml/log.xml')//logentry[@procedure=$thisproc and @id=$thisvar][last()]">
               <xsl:value-of select="@date" />
            </xsl:for-each>
         </xsl:variable>
<itemaction>
	 <xsl:attribute name="itemid"><xsl:value-of select="$thisvar"/></xsl:attribute>
	 <xsl:if test="string-length($lastdone)>0">
	 <xsl:attribute name="date"><xsl:value-of select="$lastdone"/></xsl:attribute>
     </xsl:if>
</itemaction>
	</xsl:for-each>
</itemlist>
   </xsl:copy>
  </xsl:template>


<xsl:template match="partbit|tool|consumable">
   <xsl:variable name="thisid"><xsl:value-of select="@id"/></xsl:variable> 
   <xsl:copy>
	 <xsl:variable name="image">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisid 
	 and (@commenttype='image')][last()]">
         <xsl:value-of select="@uri" />
     </xsl:for-each>
	 </xsl:variable>
	 <xsl:if test="string-length($image)>0">
	 	 <xsl:attribute name="image"><xsl:value-of select="$image"/></xsl:attribute>
     </xsl:if>

	 <xsl:variable name="newvalue">
	 <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisid 
	 and (@commenttype='quote') and string-length(@price)>0][last()]">
         <xsl:value-of select="@price" />
     </xsl:for-each>
	 </xsl:variable>
	 <xsl:if test="string-length($newvalue)>0">
	 	 <xsl:attribute name="newvalue"><xsl:value-of select="$newvalue"/></xsl:attribute>
     </xsl:if>
  
    
     <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\UBUCManual.xml" htmlbaseurl="" outputurl="..\xml\FirstPass.xml" processortype="saxon8" useresolver="no" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->