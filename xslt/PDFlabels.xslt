<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:math="http://exslt.org/math" xmlns:xlink="http://www.w3.org/1999/xlink" extension-element-prefixes="math">

<!--xsl:import href="../math/math.xsl" /-->

<!-- A3 Setting -->
<!--xsl:variable name="pageheight">420</xsl:variable>
<xsl:variable name="pagewidth">297</xsl:variable-->
<!--A4 setting -->
<xsl:variable name="pageheight">297</xsl:variable>
<xsl:variable name="pagewidth">210</xsl:variable>
<xsl:variable name="margin">10</xsl:variable>
<xsl:variable name="units">mm</xsl:variable>
<xsl:variable name="pixeldensity">2.777</xsl:variable>


<!--xsl:variable name="horizontalspace">57.15</xsl:variable>
<xsl:variable name="verticalspace">57.15</xsl:variable>
<xsl:variable name="radius">25</xsl:variable-->

<xsl:variable name="horizontalspace">50</xsl:variable>
<xsl:variable name="verticalspace">50</xsl:variable>
<xsl:variable name="radius">20</xsl:variable>
<xsl:variable name="lradius">13</xsl:variable>
<!-- Calculated variables -->
<xsl:variable name="imageboxy" select="($pageheight)-(2*$margin)"/>
<xsl:variable name="imageboxx" select="($pagewidth)-(2*$margin)"/>
<!--xsl:variable name="horizontalcorrection" select="$radius*0.866"/>
<xsl:variable name="verticalcorrection" select="$radius*0.5"/-->

<xsl:variable name="horizontalcorrection" select="$radius*0.985"/>
<xsl:variable name="verticalcorrection" select="$radius*0.174"/>

<xsl:variable name="bhorizontalcorrection" select="$radius*0.839"/>
<xsl:variable name="bverticalcorrection" select="$radius*0.545"/>


<xsl:variable name="shorizontalcorrection" select="$lradius*0.707"/>
<xsl:variable name="sverticalcorrection" select="$lradius*0.707"/>

<xsl:variable name="numinrow" select="floor((($imageboxx) - 2.2*($radius)) div ($horizontalspace))+1"/>
<xsl:variable name="numincol" select="floor((($imageboxy) - 2.2*($radius)) div ($verticalspace))+1"/>
<xsl:variable name="numonpage" select="$numincol*$numinrow"/>








<!-- end of calculated variables -->


 <xsl:template match="/">



<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

 <fo:layout-master-set>



<fo:simple-page-master master-name="main" page-height="{$pageheight}{$units}" page-width="{$pagewidth}{$units}" 
font-family="sans-serif" margin-top="{$margin}{$units}" margin-bottom="{$margin}{$units}" margin-left="{$margin}{$units}" margin-right="{$margin}{$units}">


<fo:region-body  />



<!--fo:region-before extent="1.5cm" /-->
</fo:simple-page-master>
</fo:layout-master-set>

 <fo:page-sequence master-reference="main">
<fo:flow flow-name="xsl-region-body"> 

    <xsl:apply-templates select="//itemgroup[@id='colregs']/item" mode="layout"/>
    <!--xsl:apply-templates select="//item" mode="layout"/-->


</fo:flow>
</fo:page-sequence>

 </fo:root>

 </xsl:template> 

<xsl:template match="item" mode="layout">
<xsl:variable name="pagepos" select="(position()-1) mod $numonpage"/>
<xsl:variable name="pos" select="$pagepos mod $numinrow"/>
<xsl:variable name="row" select="($pagepos - $pos) div $numinrow"/>
 
<xsl:variable name="leftoffset"><xsl:value-of select="1.2*$radius+$horizontalspace*$pos"/></xsl:variable>
<xsl:variable name="downoffset"><xsl:value-of select="1.2*$radius+$verticalspace*$row"/></xsl:variable>
<xsl:variable name="smallcx" select="($leftoffset+($horizontalspace div 2))"/>
<xsl:variable name="smallcy" select="($downoffset+($verticalspace div 2))"/>


<!--xsl:variable name="leftoffsetl" select="($leftoffset)-($horizontalcorrection)"/>
<xsl:variable name="topoffset" select="($downoffset)+($verticalcorrection)"/-->
<xsl:variable name="imagex" select="($leftoffset)-0.5*$radius"/>
<xsl:variable name="imagey" select="($downoffset)-0.5*$radius"/>
<xsl:variable name="url">http://ubuc.org/maintenance/Manual.php?id=<xsl:value-of select="@id"/></xsl:variable>
<xsl:variable name="qr">http://www.mobile-barcodes.com/qr-code-generator/generator.php?str=<xsl:value-of select="$url"/>&amp;barcode=url</xsl:variable>
<!--xsl:variable name="qr">Millie.png</xsl:variable-->

<xsl:if test="$pagepos = 0">
<xsl:variable name="vbox">0<text>  </text>0<text>  </text><xsl:value-of select="$imageboxx"/><text>  </text><xsl:value-of select="$imageboxy"/></xsl:variable>
<xsl:variable name="header">
&lt;fo:block-container  height="<xsl:value-of select="$imageboxy"/><xsl:value-of select="$units"/>" width="<xsl:value-of select="$imageboxx"/><xsl:value-of select="$units"/>"&gt;
   &lt;fo:block border="1pt solid blue" page-break-after="always"&gt;
        &lt;fo:instream-foreign-object height="<xsl:value-of select="$imageboxy"/><xsl:value-of select="$units"/>" width="<xsl:value-of select="$imageboxx"/><xsl:value-of select="$units"/>"&gt;

          &lt;svg xmlns="http://www.w3.org/2000/svg"  height="<xsl:value-of select="$imageboxy"/><xsl:value-of select="$units"/>" width="<xsl:value-of select="$imageboxx"/><xsl:value-of select="$units"/>"  preserveAspectRatio="xMinYMin slice" border="1pt solid red"&gt;
    &lt;defs&gt;

</xsl:variable>
<xsl:value-of select="$header" disable-output-escaping="yes"/>
<!-- insert position defs for all pages on path -->
<xsl:call-template name="for-loop">
    <xsl:with-param name="i"   select="0"/>
	<xsl:with-param name="increment" select="1"/>
    <xsl:with-param name="testValue"   select="$numonpage"/>
</xsl:call-template>

<xsl:variable name="nd">&lt;/defs&gt;</xsl:variable>
<xsl:value-of select="$nd" disable-output-escaping="yes"/>

</xsl:if>

<!--xsl:variable name="gmp">M<xsl:value-of select="$leftoffsetl*$pixeldensity"/>,<xsl:value-of select="$topoffset*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="0.9*$radius*$pixeldensity"/>,<xsl:value-of select="0.9*$radius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 0,0  <text> </text> <xsl:value-of select="(2*$horizontalcorrection*$pixeldensity)"/>,0</xsl:variable--> 

               <circle cx="{$leftoffset*$pixeldensity}" cy="{$downoffset*$pixeldensity}" r="{$radius*$pixeldensity}" stroke="black" stroke-width="1" fill="white"/>
			   <image x="{$imagex*$pixeldensity}" y="{$imagey*$pixeldensity}" height="{$radius*$pixeldensity}" width="{$radius*$pixeldensity}" xlink:href="{$qr}"/>

               <xsl:variable name="ltexty" select="$imagey+1.15*$radius"/>


	          <!--text x="{$leftoffset*$pixeldensity}" y="{$imagey*$pixeldensity}" style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:1.5em;"><xsl:value-of select="@name"/></text-->	
		
	          <text style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:1.5em;"><textPath xlink:href="#P{$pagepos}_name" startOffset="50%"><xsl:value-of select="@name"/></textPath></text>

			   <text x="{$leftoffset*$pixeldensity}" y="{$ltexty*$pixeldensity}" style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:1.5em;">id=<xsl:value-of select="@id"/></text>	
				  
				  
		       <text  style="font-family: 'Super Sans', Helvetica, sans-serif;
                   font-style: normal; font-size:0.7em;" text-anchor="middle"><textPath xlink:href="#P{$pagepos}_down" startOffset="50%"><xsl:value-of select="$url"/></textPath></text>
			    <text  style="font-family: 'Super Sans', Helvetica, sans-serif;
                   font-style: normal; font-size:0.7em" text-anchor="middle"><textPath xlink:href="#P{$pagepos}_up" startOffset="50%" >University of Bristol Underwater Club</textPath></text>
<!-- Small Label -->
               <circle cx="{$smallcx*$pixeldensity}" cy="{$smallcy*$pixeldensity}" r="{$lradius*$pixeldensity}" stroke="black" stroke-width="1" fill="white"/>
               <xsl:variable name="smallexp" select="1.25"/>
			   <image x="{($smallcx - (0.5*$lradius*$smallexp))*$pixeldensity}" y="{($smallcy - (0.5*$lradius*$smallexp))*$pixeldensity}" height="{$lradius*$smallexp*$pixeldensity}" width="{$lradius*$smallexp*$pixeldensity}" xlink:href="{$qr}"/>

		       <!--text  style="font-family: 'Super Sans', Helvetica, sans-serif;
                   font-style: normal; font-size:1.0em;" text-anchor="middle"><textPath xlink:href="#S{$pagepos}_down" startOffset="50%"><xsl:value-of select="@id"/></textPath></text-->

			   <text x="{$smallcx*$pixeldensity}" y="{($smallcy+0.7*$lradius)*$pixeldensity}" style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:0.9em;"><xsl:value-of select="@id"/></text>	

			   <text x="{($smallcx -(0.7*$lradius))*$pixeldensity}" y="{$smallcy*$pixeldensity}" transform="rotate(90 {($smallcx -(0.7*$lradius))*$pixeldensity},{$smallcy*$pixeldensity})" style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:0.9em;">UBUC</text>	
<!-- x="{($smallcx -(0.7*$lradius))*$pixeldensity}" y="{$smallcy*$pixeldensity}" -->
			    <!--text  style="font-family: 'Super Sans', Helvetica, sans-serif;
                   font-style: normal; font-size:0.9em" text-anchor="middle"><textPath xlink:href="#S{$pagepos}_up" startOffset="50%" ><xsl:value-of select="@name"/></textPath></text-->
			    <text  style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:0.9em;" text-anchor="middle"><textPath xlink:href="#S{$pagepos}_up" startOffset="50%" ><xsl:value-of select="@name"/></textPath></text>

			   <!--text x="{($leftoffset+$horizontalspace div 2)*$pixeldensity}" y="{($downoffset+($verticalspace div 2))*$pixeldensity}" style="text-anchor: middle; font-family: 'Super Sans', Helvetica, sans-serif;
                  font-weight: bold; font-style: normal; font-size:1.5em;"><xsl:value-of select="@id"/>Hello</text-->	
<!--xsl:value-of select="($leftoffset+$horizontalspace/2)*$pixeldensity"/>-<xsl:value-of select="($downoffset+$verticalspace/2)*$pixeldensity"/-->

<xsl:if test="($pagepos = ($numonpage -1)) or position() = last()">
<xsl:variable name="en">
&lt;/svg&gt;
       
&lt;/fo:instream-foreign-object&gt;
&lt;/fo:block&gt;
&lt;/fo:block-container&gt;

</xsl:variable>
<xsl:value-of disable-output-escaping="yes" select="$en"/>
</xsl:if>     

</xsl:template>

<xsl:template name="for-loop">
    <xsl:param name="i"      select="1"/>
	<xsl:param name="increment" select="1"/>
	<xsl:param name="testValue" select="1"/>
	<xsl:param name="iteration" select="1"/>
	<!--xsl:param name="leftoffsetl"/>
	<xsl:param name="topoffset"/-->

	<xsl:variable name="testPassed">
	<xsl:if test="$i &lt; $testValue">
		      <xsl:text>true</xsl:text>
    </xsl:if>
	</xsl:variable>

    <xsl:if test="$testPassed='true'">
<xsl:variable name="pagepos" select="$i"/>
<xsl:variable name="pos" select="$pagepos mod $numinrow"/>
<xsl:variable name="row" select="($pagepos - $pos) div $numinrow"/>

 
<xsl:variable name="leftoffset"><xsl:value-of select="1.2*$radius+$horizontalspace*$pos"/></xsl:variable>
<xsl:variable name="downoffset"><xsl:value-of select="1.2*$radius+$verticalspace*$row"/></xsl:variable>
<xsl:variable name="smallcx" select="($leftoffset+($horizontalspace div 2))"/>
<xsl:variable name="smallcy" select="($downoffset+($verticalspace div 2))"/>


<xsl:variable name="ClubRadius">0.89</xsl:variable>
<xsl:variable name="urlRadius">0.95</xsl:variable>
<xsl:variable name="nameRadius">0.6</xsl:variable>

<xsl:variable name="smallNameRadius">0.75</xsl:variable>
<xsl:variable name="smallIDRadius">0.85</xsl:variable>

<!--xsl:variable name="leftoffsetl" select="($leftoffset)-($horizontalcorrection)"/-->
<!--xsl:variable name="topoffset" select="($downoffset)+($verticalcorrection)"/-->
<!--xsl:variable name="imagex" select="($leftoffset)-0.5*$radius"/>
<xsl:variable name="imagey" select="($downoffset)-0.5*$radius"/-->

<xsl:variable name="gmpup">M<xsl:value-of select="(($leftoffset)-($ClubRadius*$horizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($downoffset)+($ClubRadius*$verticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$ClubRadius*$radius*$pixeldensity"/>,<xsl:value-of select="$ClubRadius*$radius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 1,1  <text> </text> <xsl:value-of select="(2*$ClubRadius*$horizontalcorrection*$pixeldensity)"/>,0</xsl:variable> 

<xsl:variable name="gmpName">M<xsl:value-of select="(($leftoffset)-($nameRadius*$bhorizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($downoffset)+($nameRadius*$bverticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$nameRadius*$radius*$pixeldensity"/>,<xsl:value-of select="$nameRadius*$radius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 1,1  <text> </text> <xsl:value-of select="(2*$nameRadius*$bhorizontalcorrection*$pixeldensity)"/>,0</xsl:variable> 

<xsl:variable name="gmpdown">M<xsl:value-of select="(($leftoffset)-($urlRadius*$horizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($downoffset)+($urlRadius*$verticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$urlRadius*$radius*$pixeldensity"/>,<xsl:value-of select="$urlRadius*$radius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 0,0  <text> </text> <xsl:value-of select="(2*$urlRadius*$horizontalcorrection*$pixeldensity)"/>,0</xsl:variable> 

<!--xsl:variable name="smlup">M<xsl:value-of select="(($smallcx)-($smallNameRadius*$shorizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($smallcy)+($smallNameRadius*$sverticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$smallNameRadius*$lradius*$pixeldensity"/>,<xsl:value-of select="$smallNameRadius*$lradius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 1,1  <text> </text> <xsl:value-of select="(2*$smallNameRadius*$shorizontalcorrection*$pixeldensity)"/>,0</xsl:variable--> 

<xsl:variable name="smlup">M<xsl:value-of select="(($smallcx)-($smallNameRadius*$shorizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($smallcy)-($smallNameRadius*$sverticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$smallNameRadius*$lradius*$pixeldensity"/>,<xsl:value-of select="$smallNameRadius*$lradius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 1,1  <text> </text> <xsl:value-of select="(2*$smallNameRadius*$shorizontalcorrection*$pixeldensity)"/>,<xsl:value-of select="(2*$smallNameRadius*$sverticalcorrection*$pixeldensity)"/> </xsl:variable>
<xsl:variable name="smldown">M<xsl:value-of select="(($smallcx)-($smallIDRadius*$shorizontalcorrection))*$pixeldensity"/>,<xsl:value-of select="(($smallcy)+($smallIDRadius*$sverticalcorrection))*$pixeldensity"/>
 <text>  </text> a<xsl:value-of select="$smallIDRadius*$lradius*$pixeldensity"/>,<xsl:value-of select="$smallIDRadius*$lradius*$pixeldensity"/> <text>  </text> 0 <text>  </text> 0,0  <text> </text> <xsl:value-of select="(2*$smallIDRadius*$shorizontalcorrection*$pixeldensity)"/>,0</xsl:variable> 


       <path id="P{$i}_up"
              d="{$gmpup}"/>

       <path id="P{$i}_down"
              d="{$gmpdown}"/>

       <path id="P{$i}_name"
              d="{$gmpName}"/>

       <path id="S{$i}_up"
              d="{$smlup}"/>

       <path id="S{$i}_down"
              d="{$smldown}"/>


	<xsl:call-template name="for-loop">
	    <xsl:with-param name="i" select="$i+$increment"/>
		<xsl:with-param name="increment" select="$increment"/>
	    <xsl:with-param name="testValue" select="$testValue"/>
		<xsl:with-param name="iteration" select="$iteration+1"/>
	</xsl:call-template>
	</xsl:if>
 </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\UBUCManual.xml" htmlbaseurl="" outputurl="test.fo" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->