<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:param name="CurrentDate"/>

<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>
<xsl:template match="/">
<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61486837-1', 'auto');
  ga('send', 'pageview');

</script>
<style>br{mso-data-placement:same-cell;}
</style>
</head>
<body>
<table border="1">
<tr>
<th>ID</th>
<th>Name</th>
<th>Type</th>
<th>Category</th>
<th>Status</th>
<th>Responsible Officers</th>
<th>mass</th>
<th>New Cost</th>
<th>Current Value</th>
<th>Repair Cost</th>
<th>Manufacturer</th>
<th>Year of manufacture</th>
<th>Model</th>
<th>Serial Number</th>
<th>Other Attributes</th>

<xsl:for-each select="//procedure[@frequency&gt;0]">
<xsl:sort select="@description"/>
<xsl:if test="not(preceding::procedure/@description = @description)">
<th><xsl:value-of select="@description"/></th>
</xsl:if>
</xsl:for-each>



</tr>
<xsl:for-each select="//item">
<xsl:if test="@status!='decomissioned'">
<tr>

<td>
 <xsl:variable name="generalid"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
 <a href="{$generalid}"><xsl:value-of select="@id"/></a></td>
<td><xsl:value-of select="@name"/></td>

<td><xsl:value-of select="ancestor::itemgroup/@name"/></td>
<td><xsl:value-of select="ancestor::collection[@name][1]/@name" /> </td>
<td><xsl:value-of select="@status"/></td>
<td><xsl:value-of select="ancestor::collection[@officer][1]/@officer" /> </td>
<td><xsl:value-of select="@mass"/></td>
<td><xsl:value-of select="@newvalue"/></td>
<td><xsl:value-of select="@currentvalue"/></td>
<td><xsl:value-of select="@repaircost"/></td>
<td><xsl:value-of select="@manufacturer"/></td>
<td><xsl:value-of select="@year"/></td>
<td><xsl:value-of select="@model"/></td>
<td><xsl:value-of select="@serial"/></td>
<td>
   
                 <xsl:for-each select="(@*)">
                     <xsl:sort select="name()" />

                     <xsl:if test="name() != 'id' 
					               and name() != 'name' 
								   and name() != 'image'
								   and name() != 'status'
								   and name() != 'mass'
								   and name() != 'newvalue'
								   and name() != 'currentvalue'
								   and name() != 'repaircost'
								   and name() != 'year'
								   and name() != 'serial'
								   and name() != 'manufacturer'
								   and name() != 'model'">
                        
                              <xsl:value-of select="name()" />
                           =
                              <xsl:value-of select="." />
                         <br/>
                     </xsl:if>
                  </xsl:for-each>

				<xsl:for-each select="component">
				  <xsl:value-of select="@name"/><br/>

               
                 <xsl:for-each select="(@*)">
                     <xsl:sort select="name()" />

                     <xsl:if test="name() != 'id' and name() != 'name' and name() != 'image'">
                        
                              - <xsl:value-of select="name()" />
                           =
                              <xsl:value-of select="." />
                         <br/>
                      </xsl:if>
                  </xsl:for-each>
				
				</xsl:for-each>

	
    


</td>
<xsl:variable name="thisid" select="@id"/>
<xsl:for-each select="//procedure[@frequency&gt;0]">
<xsl:sort select="@description"/>
<xsl:if test="not(preceding::procedure/@description = @description)">
<xsl:variable name="thisdescription" select="@description"/>
<td>
<xsl:variable name="procid">
<xsl:for-each select="//item[@id=$thisid]/ancestor::itemgroup/procedure[@description=$thisdescription]|//item[@id=$thisid]/ancestor::collection/procedure[@description=$thisdescription]">
<xsl:value-of select="@id"/>
</xsl:for-each>
</xsl:variable>
<xsl:variable name="frequency">
<xsl:value-of select="//procedure[@id=$procid]/@frequency"/>
</xsl:variable>

<xsl:if test="$procid=''">
N/A
</xsl:if>

<xsl:if test="$procid!=''">
      <xsl:variable name="lastDate">
         <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisid and @procedure=$procid][last()]">
            <xsl:value-of select="@date" />
         </xsl:for-each>
      </xsl:variable>
<xsl:variable name="datestr">
<xsl:if test="$lastDate=''">
never logged
</xsl:if>
<xsl:if test="$lastDate!=''">
<xsl:value-of select="$lastDate"/> <!--xsl:value-of select="$frequency"/-->
</xsl:if>
</xsl:variable>
<xsl:variable name="proclink"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$procid" /></xsl:variable>
<a href="{$proclink}"><xsl:value-of select="$datestr" /></a>



</xsl:if>


</td>
</xsl:if>
</xsl:for-each>


</tr>
</xsl:if>
</xsl:for-each>
</table>
</body>
</html>	
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\FirstPass.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->