<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:param name="CurrentDate"/>
<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>


<xsl:template match="/">
<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61486837-1', 'auto');
  ga('send', 'pageview');

</script>
<style>br{mso-data-placement:same-cell;}
</style>
</head>
<body>
<table border="1">
<tr>
<th>ID</th>
<th>Name</th>
<th>Type</th>
<th>Responsible Officers</th>
<th>New Cost</th>
<th>Quantity</th>
<th>Other Attributes</th>
<th>Procedures</th>
</tr>
<xsl:for-each select="//tool|//consumable|//partbit|//item">
<xsl:sort select="@name" order="ascending" />
<xsl:if test="(name()='item' and @status != 'decomissioned') or @store='true'">
 <xsl:apply-templates select="." mode="table" />
</xsl:if>
 <!--xsl:value-of select="@name"/-->
</xsl:for-each>
</table>
</body>
</html>	
</xsl:template>

<!--xsl:template match="partbit|consumable|tool" mode="table">
<xsl:value-of select="@name"/>
</xsl:template-->

<xsl:template match="partbit|consumable|tool|item" mode="table">
      <xsl:variable name="thisitem01">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <tr>
	     <td>
		 
		  <xsl:variable name="generalid"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
 <a href="{$generalid}"><xsl:value-of select="@id"/></a></td>


         <td>
             <xsl:value-of select="@name"/>
         </td>
		 <td><xsl:value-of select="name()"/></td>
		 <td>
	
		 	
		<xsl:variable name="officer">
               <xsl:for-each select="(//toolref[@id=$thisitem01]|//partref[@id=$thisitem01]|//consumableref[@id=$thisitem01]|//item[@id=$thisitem01])[1]">
                  <xsl:value-of select="ancestor::collection[@officer][1]/@officer" />
               </xsl:for-each>
		   </xsl:variable>
		   <xsl:value-of select="$officer"/>

		 
		 </td>
		
         <td><xsl:value-of select="@newvalue"/></td>
		 <td></td>
      
	     <td>
               
                 <xsl:for-each select="(@*)">
                     <xsl:sort select="name()" />

                     <xsl:if test="name() != 'id' 
					               and name() != 'name' 
								   and name() != 'image'
								   and name() != 'store'
								   and name() != 'newvalue'">
                        
                              <xsl:value-of select="name()" />
                           =
                              <xsl:value-of select="." />
                         <br/>
                     </xsl:if>
                  </xsl:for-each>

            
         </td>

         <td>
            
			<xsl:variable name="referencedProcs">

            <xsl:if test="name()!='item'">
               <xsl:for-each select="//toolref[@id=$thisitem01]|//partref[@id=$thisitem01]|//consumableref[@id=$thisitem01]|//item[@id=$thisitem01]/ancestor::itemgroup/procedure|//item[@id=$thisitem01]/ancestor::collection/procedure">
                  <xsl:for-each select="ancestor::procedure">,<xsl:value-of select="@id"/>,</xsl:for-each>
               </xsl:for-each>
		   </xsl:if>

            <xsl:if test="name()='item'">
               <xsl:for-each select="ancestor::itemgroup/procedure|ancestor::collection/procedure">
                  ,<xsl:value-of select="@id"/>,
               </xsl:for-each>
		   </xsl:if>
		   </xsl:variable>
		   



		   <xsl:for-each select="//procedure"> 
		   <xsl:sort select="@name" order="ascending"/>
		       <xsl:variable name="idcheck">,<xsl:value-of select="@id"/>,</xsl:variable>
               <xsl:if test="contains($referencedProcs,$idcheck)">
                   <xsl:variable name="proclink"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>
                  
                     
                       <a href="{$proclink}">
                           <xsl:value-of select="@name" />
                        </a>
                     <br/>
					 </xsl:if>
               </xsl:for-each>
            
         </td>

      </tr>
	  
   </xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\UBUCManual.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->