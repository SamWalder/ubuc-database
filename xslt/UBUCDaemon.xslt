<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- The Daemon will produce a list of comments which are for adding to the log. The comments will be made by assessing whether any of the procedures are out of date -->
	<xsl:output method="xml" indent="yes" />

	<xsl:param name="id"/>
	<xsl:param name="CurrentDate">2015-06-20</xsl:param>
	<xsl:variable name="now" select="(12*substring($CurrentDate,1,4))+substring($CurrentDate,6,2)-1"/>

	<xsl:template match="/">
	<!-- Create a template to match the whole document -->
		<newlogentries>
			<!-- Apply the item template to item nodes which are not decomissioned or missing -->
			<xsl:apply-templates select="//item[(@status != 'decomissioned' and @status != 'missing')]" mode="check"/>
		</newlogentries>
	</xsl:template>


	<xsl:template match="item" mode="check">
	<!-- For each item we need to determine the current status and whether costs of out of date procedures exceeds the current logged repair cost -->
		<xsl:variable name="itemid" select="@id"/>

        <!-- Create a list of all of the out of date proceedures on this item. If there are no out of date proceedures (resulting in this variable being empty), then this item is allowed to be marked as wroking -->
		<xsl:variable name="AllowedWorking">
	        <!-- Go through all the proceedures on the parent node of this item -->
			<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and ( @requirement = 'recommended' or @requirement = 'required' or @requirement = 'mandatory' ) ]|ancestor::collection/procedure[@frequency &gt; 0 and ( @requirement = 'recommended' or @requirement = 'required' or @requirement = 'mandatory' )  ]">
				<xsl:variable name="procid" select="@id"/>

				<!-- Get the last log node relating to this proccedure and store the date of it -->
				<xsl:variable name="lastDate">
					<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
						<xsl:value-of select="@date"/>
					</xsl:for-each>
				</xsl:variable>

				<!-- If the date variable is not empty -->
				<xsl:if test="$lastDate != ''">
				    <!-- Get the year and month this was done -->
					<xsl:variable name="year">
						<xsl:value-of select="substring($lastDate,1,4)"/>
					</xsl:variable>
					<xsl:variable name="month">
						<xsl:value-of select="substring($lastDate,6,2)"/>
					</xsl:variable>
					<xsl:variable name="lastactual" select="(12*$year)+$month -1"/>
					<!-- If we are now past the due datae of the proceedure output the (proceedure?) ID -->
					<xsl:if test="$now &gt; ($lastactual+@frequency)">,<xsl:value-of select="@id"/>,</xsl:if>
				</xsl:if>

				<!-- If the date variable is empty, the proceedure has never been compleated, so output the (proceedure?) id -->
				<xsl:if test="$lastDate = ''">,<xsl:value-of select="@id"/>,</xsl:if>
			</xsl:for-each>
		</xsl:variable>

		<xsl:variable name="AllowedNiggle">
			<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and ( @requirement = 'required' or @requirement = 'mandatory' ) ]|ancestor::collection/procedure[@frequency &gt; 0 and ( @requirement = 'required' or @requirement = 'mandatory' )  ]">
				<xsl:variable name="procid" select="@id"/>
				<xsl:variable name="lastDate">
					<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
						<xsl:value-of select="@date"/>
					</xsl:for-each>
				</xsl:variable>
				<xsl:if test="$lastDate != ''">
					<xsl:variable name="year">
						<xsl:value-of select="substring($lastDate,1,4)"/>
					</xsl:variable>
					<xsl:variable name="month">
						<xsl:value-of select="substring($lastDate,6,2)"/>
					</xsl:variable>
					<xsl:variable name="lastactual" select="(12*$year)+$month -1"/>
					<xsl:if test="$now &gt; ($lastactual+@frequency)">,<xsl:value-of select="@id"/>,</xsl:if>
				</xsl:if>
				<xsl:if test="$lastDate = ''">,<xsl:value-of select="@id"/>,</xsl:if>
			</xsl:for-each>
		</xsl:variable>

		<xsl:variable name="AllowedDamaged">
			<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and ( @requirement = 'mandatory' ) ]|ancestor::collection/procedure[@frequency &gt; 0 and ( @requirement = 'mandatory' )  ]">
				<xsl:variable name="procid" select="@id"/>
				<xsl:variable name="lastDate">
					<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
						<xsl:value-of select="@date"/>
					</xsl:for-each>
				</xsl:variable>
				<xsl:if test="$lastDate != ''">
					<xsl:variable name="year">
						<xsl:value-of select="substring($lastDate,1,4)"/>
					</xsl:variable>
					<xsl:variable name="month">
						<xsl:value-of select="substring($lastDate,6,2)"/>
					</xsl:variable>
					<xsl:variable name="lastactual" select="(12*$year)+$month -1"/>
					<xsl:if test="$now &gt; ($lastactual+@frequency)">,<xsl:value-of select="@id"/>,</xsl:if>
				</xsl:if>
				<xsl:if test="$lastDate = ''">,<xsl:value-of select="@id"/>,</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:variable name="BestStatus">
			<xsl:choose>
				<xsl:when test="$AllowedDamaged != ''">broken</xsl:when>
				<xsl:when test="$AllowedNiggle != ''">damaged</xsl:when>
				<xsl:when test="$AllowedWorking != ''">niggle</xsl:when>
				<xsl:otherwise><xsl:value-of select="@status"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="ToAlter">
			<xsl:choose>
				<xsl:when test="@status='working' and $AllowedWorking != ''">
					<xsl:value-of select="$AllowedWorking"/>
				</xsl:when>
				<xsl:when test="@status='niggle' and $AllowedNiggle != ''">
					<xsl:value-of select="$AllowedWorking"/>
				</xsl:when>
				<xsl:when test="@status='damaged' and $AllowedDamaged != ''">
					<xsl:value-of select="$AllowedWorking"/>
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:if test="$AllowedWorking != ''">

			<xsl:variable name="proclist">
				<xsl:call-template name="price">
					<xsl:with-param name="mode">namestr</xsl:with-param>
					<xsl:with-param name="index" select="0" />
					<xsl:with-param name="remainingstring" select="$ToAlter" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="repairprice">
				<xsl:call-template name="price">
					<xsl:with-param name="mode" select="price"/>
					<xsl:with-param name="index" select="0" />
					<xsl:with-param name="remainingstring" select="$ToAlter" />
				</xsl:call-template>
			</xsl:variable>

			<xsl:if test=" ($repairprice &gt; @repaircost) or $ToAlter != ''">
				<logentry date="{$CurrentDate}" name="UBUC Daemon" id="{$itemid}" commenttype="{$BestStatus}" price="{$repairprice}"><xsl:value-of select="$proclist"/> out of date</logentry>
			</xsl:if>
		</xsl:if>

	</xsl:template>

	<xsl:template name="price">
		<xsl:param name="mode" select="namestr"/>
		<xsl:param name="index" select="0" />
		<xsl:param name="total" select="0" />
		<xsl:param name="remainingstring"/>
		<xsl:param name="constructedstring" select="''"/>
		<xsl:variable name="procid">
			<xsl:value-of select="substring-before($remainingstring,',')"/>
		</xsl:variable>

		<xsl:variable name="pname">
			<xsl:if test="$procid != ''">
				<xsl:value-of select="//procedure[@id=$procid]/@name"/>;
			</xsl:if>
			<xsl:if test="not($procid != '')">
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="pprice">
			<xsl:if test="$procid != ''">
				<xsl:choose>
					<xsl:when test="//procedure[@id=$procid]/@BudgetEstimate != ''">
						<xsl:value-of select="$total+//procedure[@id=$procid]/@BudgetEstimate"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$total"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="not($procid != '')">
				<xsl:value-of select="$total"/>
			</xsl:if>
		</xsl:variable>
		
		<xsl:if test="not(substring-after($remainingstring,',')='')">
			<xsl:call-template name="price">
				<xsl:with-param name="mode" select="$mode"/>
				<xsl:with-param name="index" select="$index + 1" />
				<xsl:with-param name="total" select="$pprice"/>
				<xsl:with-param name="remainingstring" select="substring-after($remainingstring,',')" />
				<xsl:with-param name="constructedstring">
					<xsl:value-of select="$constructedstring"/>
					<xsl:value-of select="$pname"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="substring-after($remainingstring,',')=''">
			<xsl:choose>
				<xsl:when test="$mode = 'namestr'">
					<xsl:value-of select="$constructedstring"/><xsl:value-of select="$pname"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$pprice"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\FirstPass.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->