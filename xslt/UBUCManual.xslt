<?xml version="1.0" encoding="iso-8859-1"?>
<!-- UBUCManual.xlst
	This file forms the template for displaying the details about a single item or the home page.
  -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:err="http://www.w3.org/2005/xqt-errors" 
 xmlns:itemlookup="http://ubuc.org/maintenance/item"   exclude-result-prefixes="xs xdt err fn">
	<xsl:output method="html" indent="yes" />

	<xsl:param name="id"/>
	<xsl:param name="CurrentDate"/>

	<xsl:variable name="nameattribute">name</xsl:variable>
	<xsl:variable name="empty"></xsl:variable>
	<xsl:variable name="training">Training</xsl:variable>
	<xsl:variable name="general">General</xsl:variable>

	<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>
	<xsl:variable name="logphp">http://ubuc.org/maintenance/Log.php</xsl:variable>
	<xsl:variable name="constlink">http://ubuc.org/index.php/club-constitution/</xsl:variable>
	<xsl:variable name="qrgen">http://www.mobile-barcodes.com/qr-code-generator/generator.php?str=http://ubuc.org/maintenance/Manual.php?id</xsl:variable>

	<xsl:template match="/">  
     
    <html>
        <head>
			<!-- J Query script declarations. These enable java script functionality like pop up boxes on mouse over -->
			<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
			<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="./javascript/nhpup_1.1.js"></script>

			<!-- Style section contains CSS layout instructions -->
			<style>   
				<!-- Mouse over pop-up box formatting -->
				#pup {position:absolute;
					z-index:200; /* aaaalways on top*/
					padding: 3px;
					margin-left: 10px;
					margin-top: 5px;
					width: 250px;
					border: 1px solid black;
					background-color: #777;
					color: white;
					font-size: 0.95em;
					}     
			</style>

			<link rel="stylesheet" type="text/css" href="css/UBUCManual.css"></link>
		 
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-61486837-1', 'auto');
				ga('send', 'pageview');
			</script>
        </head>

		<!-- HTML body section displaying the UBUC manual -->
        <body>

			<h1>UBUC Maintenance Documentation</h1>
			<p><a href="{$constlink}">This club exists to provide for its members  facilities, opportunity and training for underwater exploration, science, and sport.</a></p>
			
			<!-- If no parameter is defined, then show the main home screen -->
			<xsl:if test="string-length($id)=0">
			<A NAME="plan" />

			<h2>Introduction</h2>
			UBUC has lots of gear to allow its members to visit the underwater world, and to transport members offshore to exciting sites. This valuable equipment needs to be maintained and this page documents our maintenance procedures.
			
			<h2>Contents</h2>
			<ul>
				<li><a href="http://ubuc.org/maintenance/html/help.html">Manual Overview</a></li>
				<li>Useful sheets
					<ul>
			 			<li><xsl:variable name="loglink"><xsl:value-of select="$homelink"/>?id=log</xsl:variable>
				   		<a href="http://ubuc.org/maintenance/Manual.php?id=log">Full Log</a></li>
						<li><a href="http://ubuc.org/maintenance/Manual.php?id=sheet">Current Status Table</a></li>
						<li><a href="http://ubuc.org/maintenance/Manual.php?id=planning">Upcoming task schedule</a></li>
						<li><a href="http://ubuc.org/maintenance/Manual.php?id=stock">Stores Stocktake sheet</a></li>
					</ul>
				</li>
				<li><a href="#EquipmentState">Current Equipment State</a></li>
				<li><a href="#Scheduled">Scheduled Process Overview</a></li>
				<li>Other pages
					<ul>
						<li><a href="#General">Committee Tasks</a></li>
						<li><a href="#Training">Extended Training Needs</a></li>
					</ul>
				</li>
				<li>Utility pages
					<ul>
						<li><a href="http://ubuc.org/maintenance/editItem.php">Add New Item</a></li>
						<li><a href="http://ubuc.org/maintenance/editProcedure.php">Add New Procedure</a></li>
						<li><a href="http://ubuc.org/maintenance/editSupplier.php">Add New Supplier</a></li>
					</ul>
				</li>

				<!-- li>
				   <a href="#UBUCAssets">UBUC Assets</a>
				</li>

				<li>
				   <a href="#Procedures">Procedures</a>
				</li -->

	<!-- Embed the stores check down a level -->
				<!--li>
				   <a href="#Stores">UBUC Stores Check</a>

				   <ul>
					  <li>
						 <a href="#tools">Tools</a>
					  </li>

					  <li>
						 <a href="#consumables">Consumables</a>
					  </li>

					  <li>
						 <a href="#parts">Spares</a>
					  </li>
				   </ul>
				</li-->

				<li>
				   <a href="#Suppliers">Suppliers</a>
				</li>
			 </ul>
			<a name="#EquipmentState"/>
			<h1>Current Equipment State</h1>
			<table border="1" width="100%">
			<tr>
			<th width="10%">State</th><th width="45%">Costed Items</th><th width="45%">Uncosted Items</th>
			</tr>
			<tr bgcolor="green">
			<td onmouseover="nhpup.popup('An item in working order has no outstanding issues logged against it');">working</td><td>N/A</td><td>
			<xsl:for-each select="//item[@status='working']"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			 <tr bgcolor="yellow">
			<td onmouseover="nhpup.popup('An item with a niggle has an issue that should be fixed but does not impact its use');">niggle - 
					<xsl:value-of select="sum(//item[@status='niggle']/@repaircost)" /></td><td><xsl:for-each select="//item[@status='niggle' and string-length(@repaircost)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status='niggle' and string-length(@repaircost)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			<tr bgcolor="orange">
			<td onmouseover="nhpup.popup('A damaged item must be used with care or in limited circumstances such as pool only');">damaged - 
					<xsl:value-of select="sum(//item[@status='damaged']/@repaircost)" />
					</td><td><xsl:for-each select="//item[@status='damaged' and string-length(@repaircost)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status='damaged' and string-length(@repaircost)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			<tr bgcolor="red">
			<td onmouseover="nhpup.popup('A broken item must not be used');">broken - 
			<xsl:value-of select="sum(//item[@status='broken']/@repaircost)" />

			</td><td><xsl:for-each select="//item[@status='broken' and string-length(@repaircost)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status='broken' and string-length(@repaircost)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			<tr bgcolor="grey">
			<td onmouseover="nhpup.popup('A missing item cannot be used');">missing - 
					<xsl:value-of select="sum(//item[@status='missing']/@repaircost)" />
					</td><td><xsl:for-each select="//item[@status='missing' and string-length(@repaircost)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status='missing' and string-length(@repaircost)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			<tr bgcolor="purple">
			<td onmouseover="nhpup.popup('A decomissioned item has been thrown away or sold on');">decomissioned</td><td>N/A</td><td><xsl:for-each select="//item[@status='decomissioned']"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			</table>
			<!--h1>Capital Equipment Value</h1>
			<a NAME="Capital" />
			<table border="1" width="100%">
			<tr>
			<th width="10%">State</th><th width="45%">Valued Items</th><th width="45%">Unvalued Items</th>

			</tr>
			 <tr>
			<td onmouseover="nhpup.popup('The cost of a new replacement');">New Value - 
					<xsl:value-of select="sum(//item[@status != 'decomissioned']/@newvalue)" /></td><td><xsl:for-each select="//item[@status != 'decomissioned' and string-length(@newvalue)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status != 'decomissioned' and string-length(@newvalue)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>
			 <tr>
			<td onmouseover="nhpup.popup('The current values of our items');">Current Value - 
					<xsl:value-of select="sum(//item[@status != 'decomissioned']/@currentvalue)" /></td><td><xsl:for-each select="//item[@status != 'decomissioned' and string-length(@currentvalue)>0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td><td><xsl:for-each select="//item[@status != 'decomissioned' and string-length(@currentvalue)=0]"><xsl:apply-templates select="." mode="text"/>, </xsl:for-each></td>
			</tr>

			</table-->
			 <h1>Scheduled Process Overview</h1>

			 <a NAME="Scheduled" />

			 <table border="1">
				<tr>
				   <th>Frequency (Months)</th>
				   <th>Level</th>
				   <th>Procedure</th>
				   <th>Owner</th>
				   <th>Estimated Cost</th>
				   <th>Estimated Effort (hrs)</th>
				   <th>Items to be processed</th>
				</tr>

				<xsl:for-each select="//procedure">
				   <xsl:sort select="@frequency" order="ascending" data-type="number" />

				   <xsl:variable name="procedurename"><xsl:value-of select="@id" /></xsl:variable>

				   <xsl:if test="@frequency&gt;0">
					  <xsl:variable name="procname"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>

					  <tr>
						 <td>
							<xsl:value-of select="@frequency" />
						 </td>

						 <xsl:variable name="boxc">
							<xsl:choose>
							   <xsl:when test="@requirement='NotAdvised'">grey</xsl:when>

							   <xsl:when test="@requirement='mandatory'">red</xsl:when>

							   <xsl:when test="@requirement='required'">orange</xsl:when>

							   <xsl:when test="@requirement='recommended'">yellow</xsl:when>

							   <xsl:otherwise>transparent</xsl:otherwise>
							</xsl:choose>
						 </xsl:variable>

						 <td>
							<xsl:apply-templates select="." mode="level" />
						 </td>

						 <td bgcolor="{$boxc}" onmouseover="nhpup.popup('{@reason}');">
						 	<!-- Print the proceedure name -->
							<a href="{$procname}">
							   <xsl:value-of select="@name" />
							</a>
						 </td>

						 <td>
						 	<!-- Print the owner -->
							<xsl:value-of select="ancestor::collection[@officer][1]/@officer" />
						 </td>

						 <td>
							<xsl:value-of select="@BudgetEstimate" />
						 </td>
						 
						 <td>
							<xsl:value-of select="@effort" />
						 </td>
						 
						 <td>
						 	<!-- Print the items which need to be processed -->
							<xsl:apply-templates select="." mode="itemlist" />
						 </td>
					  </tr>
				   </xsl:if>
				</xsl:for-each>
			 </table>

			<h1>General Committee Tasks</h1>
			<a name="#General"/>
			<ul>
			<xsl:for-each select="//procedure[@description=$general]">
			<xsl:variable name="generalid"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
			<li><a href="{$generalid}"><xsl:value-of select="@name"/></a></li>
			</xsl:for-each>
			
			</ul>


			 <h1>Extended Training Needs</h1>
			<a name="#Training"/>
			<ul>
			<xsl:for-each select="//procedure[@description=$training]">
			<xsl:variable name="trainingid"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
			<li><a href="{$trainingid}"><xsl:value-of select="@name"/></a></li>
			</xsl:for-each>
			
			</ul>


			 <!-- h1>UBUC Assets</h1>

			 <a NAME="UBUCAssets" />

			 <table width="100%" border="1">
				<tr bgcolor="blue">
				   <th width="10%">Item</th>

				   <th width="5%">Details</th>

				   <th width="25%">Procedures</th>

				   <th width="60%">
					  <table width="100%">
						 <tr>
							<th>Log</th>
						 </tr>

						 <tr>
							<th width="12%">Date</th>

							<th width="8%">Price</th>

							<th>Comment</th>
						 </tr>
					  </table>
				   </th>
				</tr>

				<xsl:for-each select="//itemgroup">
				   <xsl:apply-templates select="item" mode="intable" />
				</xsl:for-each>
			 </table -->

			 <!-- h1>UBUC Procedures</h1>

			 <a NAME="Procedures" />

			 <xsl:for-each select="//procedure">
				<xsl:apply-templates select="." mode="page" />
			 </xsl:for-each -->

			 <!--a NAME="Stores" />

			 <h1>UBUC Stores Check</h1>

			 <a NAMe="tools" />

			 <h2>Tools</h2>

			 <table width="100%" border="1">
				<tr bgcolor="blue">
				   <th>Name</th>

				   <th>Item Attributes</th>

				   <th>Procedures</th>

				   <th>
					  <table width="100%">
						 <tr>
							<th>Log</th>
						 </tr>

						 <tr>
							<th width="12%">Date</th>

							<th width="8%">Price</th>

							<th>Comment</th>
						 </tr>
					  </table>
				   </th>
				</tr>

				<xsl:for-each select="//tool">
				   <xsl:sort select="@name" order="ascending" />

				   <xsl:apply-templates select="." mode="table" />
				</xsl:for-each>
			 </table>

			 <xsl:variable name="true">true</xsl:variable>

			 <a NAMe="consumables" />

			 <h2>Consumables</h2>

			 <table width="100%" border="1">
				<tr bgcolor="blue">
				   <th>Name</th>

				   <th>Item Attributes</th>

				   <th>Procedures</th>

				   <th>
					  <table width="100%">
						 <tr>
							<th>Log</th>
						 </tr>

						 <tr>
							<th width="12%">Date</th>

							<th width="8%">Price</th>

							<th>Comment</th>
						 </tr>
					  </table>
				   </th>
				</tr>

				<xsl:for-each select="//consumable[@store=$true]">
				   <xsl:sort select="@name" order="ascending" />

				   <xsl:apply-templates select="." mode="table" />
				</xsl:for-each>
			 </table>

			 <a NAME="parts" />

			 <h2>Spare Parts</h2>

			 <table width="100%" border="1">
				<tr bgcolor="blue">
				   <th>Name</th>

				   <th>Item Attributes</th>

				   <th>Procedures</th>

				   <th>
					  <table width="100%">
						 <tr>
							<th>Log</th>
						 </tr>

						 <tr>
							<th width="12%">Date</th>

							<th width="8%">Price</th>

							<th>Comment</th>
						 </tr>
					  </table>
				   </th>
				</tr>

				<xsl:for-each select="//partbit[@store=$true]">
				   <xsl:sort select="@name" order="ascending" />

				   <xsl:apply-templates select="." mode="table" />
				</xsl:for-each>
			 </table-->

			 <h1>Suppliers</h1>

			 <a NAME="Suppliers" />
			 <ul>
			 <xsl:for-each select="document('../xml/suppliers.xml')//supplier">
				<xsl:sort select="@name" />

				<li><xsl:apply-templates select="." mode="text" /></li>
			 </xsl:for-each>
			 </ul>


			 </xsl:if>
			
			<!-- If a ID parameter has been supplied then display the item page-->
			<xsl:if test="string-length($id)>0">
				<p><a href="{$homelink}">Back To Contents</a></p>

				<xsl:for-each select="//item[@id=$id] | //procedure[@id=$id] | //partbit[@id=$id] | //consumable[@id=$id] | //tool[@id=$id] | //ref[@id=$id] | document('../xml/suppliers.xml')//supplier[@id=$id]">
					<!-- The tamplates are defined later in this file -->
					<xsl:apply-templates select="." mode="page"/>
				</xsl:for-each>
			</xsl:if> 

			<!-- At the end of the document place the hidden text for tool/part and consumable pop-uips-->
			<xsl:for-each select="/manual/parts/partbit">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/tools/tool">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/consumables/consumable">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/references/ref">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="//item">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>
		</body>
    </html>
</xsl:template>

<!-- Template to apply for displaying a single "item" -->
<xsl:template match="item" mode="page">
    <xsl:variable name="thitem"><xsl:value-of select="@id"/></xsl:variable>
    <xsl:variable name="OperationalState">
		<!--xsl:for-each select="document('../xml/log.xml')//logentry[@commenttype and @id=$id][last()]">
			<xsl:value-of select="@commenttype" />
		</xsl:for-each-->
		<xsl:value-of select="@status"/>
	</xsl:variable>

	<!-- Display the name header -->
	<xsl:variable name="OperationalColour">
	<xsl:choose>
		<xsl:when test="$OperationalState='broken'">red</xsl:when>

		<xsl:when test="$OperationalState='damaged'">orange</xsl:when>

		<xsl:when test="$OperationalState='niggle'">yellow</xsl:when>

		<xsl:when test="$OperationalState='missing'">grey</xsl:when>

		<xsl:otherwise>transparent</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<h1><xsl:value-of select="@name"/> is currently <xsl:value-of select="@status"/></h1>
	
	<!-- Display a link to edit this item -->
	<xsl:variable name="editLink">http://ubuc.org/maintenance/editItem.php?id=<xsl:value-of select='@id'/></xsl:variable>
	<a href="{$editLink}">Edit this item</a>
	
	<!-- Display a link to see the QR code for this item -->
	<ul>
	<li>UBUCid=<xsl:value-of select="@id"/></li>
	<xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
	<li><a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a></li>
	</ul>

	<!-- Display the image if the item has one -->
	<xsl:if test="string-length(@image)>0">
		<h2>Image</h2>
		<a href="{@image}">
		<img src="{@image}"/></a>
	</xsl:if>

	<!-- Display a table of other attributes -->
	<h2>Other Attributes</h2>

	<table border="1" width="100%">
		<tr>
			<th>Part</th>
			<th>Attributes</th>
			<th>Applicable Procedures</th>
		</tr>
		<tr>
			<td>Main</td>
			<td>
				<ul>
					<xsl:for-each select="(@*)">
						<xsl:sort select="name()" />

						<xsl:if test="name() != 'id' and name() != 'name' and name() != 'image'">
							<li>
								<xsl:value-of select="name()" />
								=
								 <xsl:value-of select="." />
							</li>
						</xsl:if>
					</xsl:for-each>
				    <xsl:for-each select="component">
						<li><xsl:value-of select="@name"/></li>
						<ul>
						<xsl:for-each select="(@*)">
							<xsl:sort select="name()" />

							<xsl:if test="name() != 'id' and name() != 'name' and name() != 'image'">
								<li>
								<xsl:value-of select="name()" />
								=
								<xsl:value-of select="." />
								</li>
							</xsl:if>
						</xsl:for-each>
						</ul>
					</xsl:for-each>
				</ul>
			</td>
			<td>
				<table width="100%">
					<tr align='left'>
						<th>Record<br/>Complete</th>
						<th>Difficulty</th>
						<th>Procedure</th>
						<th>Last Compleated</th>
						<th>Interval (Months)</th>
						<th>Estimated Cost</th>
					</tr>
					<xsl:for-each select="ancestor::itemgroup/procedure|ancestor::collection/procedure">
						<xsl:sort select="@frequency" data-type="number"/>
						<tr>
							<td>
								<xsl:apply-templates select="." mode="intable">
									<xsl:with-param name="thisitem" select="$thitem"/>
								</xsl:apply-templates>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>

	</table>			  

	<!-- Display a table of reference material -->
	<h2>References</h2>
	<ul>
     	<xsl:for-each select="ancestor::itemgroup/refid|ancestor::collection/refid">
 			<li><xsl:apply-templates select="." mode="text"/></li>
		</xsl:for-each>
	</ul>

	<!-- Display the list of logs for this item -->
   	<xsl:variable name="loglink05"><xsl:value-of select="$logphp"/>?id=<xsl:value-of select="@id"/></xsl:variable>
   	<h2>Logs</h2>
   	<p><a href="{$loglink05}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');">Add new log entry</a></p>
	
	<table width="100%">
		<tr>
	        <th width="12%" bgcolor="lightgrey"><b>Date</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Member</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Supplier</b></th>
			<th width="8" bgcolor="lightgrey"><b>Cost</b></th>
			<th width="10%" bgcolor="lightgrey"><b>Comment Type</b></th>
			<th bgcolor="lightgrey"><b>Comment</b></th>
		</tr>
		<xsl:variable name="thisitem06"><xsl:value-of select="@id" /></xsl:variable>
		<xsl:for-each select="document('../xml/log.xml')/logfile/logentry[@id=$thisitem06]">
			<xsl:sort select="@date" order="descending" />
			<xsl:apply-templates select="." mode="intable" />
		</xsl:for-each>
	</table>
</xsl:template>


<xsl:template match="item" mode="intable">
	<!-- Displays a row of the item table for an item -->
	<xsl:variable name="thisitem05"><xsl:value-of select="@id" /></xsl:variable>
	<xsl:variable name="OperationalState">
		<xsl:value-of select="@status"/>
	</xsl:variable>
	<xsl:variable name="OperationalColour">
		<xsl:choose>
			<xsl:when test="$OperationalState='broken'">red</xsl:when>
			<xsl:when test="$OperationalState='damaged'">orange</xsl:when>
			<xsl:when test="$OperationalState='niggle'">yellow</xsl:when>
			<xsl:when test="$OperationalState='missing'">grey</xsl:when>
			<xsl:otherwise>transparent</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Start the table row for this item -->
	<xsl:if test="$OperationalState!='decomissioned'">
		<tr>
			<td bgcolor="{$OperationalColour}">
				<A NAME="{$thisitem05}" />
				<xsl:variable name="itemlink"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
				<table width="100%" table-layout="fixed">
					<tr>
						<th>
							<a href="{$itemlink}"><xsl:value-of select="@name" /></a>
						</th>
					</tr>
					<tr>
						<td>UBUC id=
						<xsl:value-of select="@id" />
						</td>
					</tr>
					<tr>
						<td>
						<xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
						<a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a>
						</td>
					</tr>
					<tr>
						<td>
						<xsl:variable name="loglink06"><xsl:value-of select="$logphp"/>?id=<xsl:value-of select="@id"/></xsl:variable>
						<a href="{$loglink06}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');"><img src="Resources/Log.gif"/></a>
						</td>
					</tr>
					<tr>Officer 
						<ul>
							<xsl:for-each select="ancestor::collection[@officer]">
								<xsl:if test="@officer">
									<li>
										<xsl:value-of select="@officer" />
									</li>
								</xsl:if>
							</xsl:for-each>
						</ul>
					</tr>
					<tr>
						<td>
							<xsl:if test="@image">
								<a target="_blank">
								<xsl:attribute name="href">
									<xsl:value-of select="@image" />
								</xsl:attribute>
								<img width="120">
								<xsl:attribute name="src">
									<xsl:value-of select="@image" />
								</xsl:attribute>
								</img>
								</a>
							</xsl:if>
						</td>
					</tr>
				</table>
			</td>
	
			<td bgcolor="{$OperationalColour}">
				<table>
					<xsl:for-each select="(@*)">
						<xsl:sort select="name()" />
		
						<xsl:if test="name() != 'id' and name() != 'name'">
							<tr>
								<td width="40%">
									<xsl:value-of select="name()" />
								</td>
		
								<td>
									<xsl:value-of select="." />
								</td>
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
	
			<xsl:for-each select="./component">
				<table>
					<tr>
						<th>Component</th>
						<th>
							<xsl:value-of select="@name" />
						</th>
					</tr>
	
					<xsl:for-each select="(@*)">
						<xsl:sort select="name()" />
						<xsl:if test="name() != 'id' and name() != 'name'">
							<tr>
								<td width="40%">
									<xsl:value-of select="name()" />
								</td>
								<td>
									<xsl:value-of select="." />
								</td>
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
			</xsl:for-each>
		</td>
	
			<td>
				<table width="100%">
					<xsl:variable name="thisitem07">
						<xsl:value-of select="@id" />
					</xsl:variable>
					
					<xsl:for-each select="child::procedure|ancestor::itemgroup/procedure|ancestor::collection/procedure">
						<xsl:sort select="@frequency" />
						<tr>
							<td>
								<xsl:apply-templates select="." mode="intable">
									<xsl:with-param name="thisitem" select="$thisitem07"/>
								</xsl:apply-templates>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
	
			<td>
				<div style="height: 200px; overflow: auto;">
					<table width="100%">
						<xsl:variable name="thisitem06"><xsl:value-of select="@id" /></xsl:variable>
						<xsl:for-each select="document('../xml/log.xml')/logfile/logentry[@id=$thisitem06]">
							<xsl:sort select="@date" order="descending" />
							<xsl:apply-templates select="." mode="intable" />
						</xsl:for-each>
					</table>
				</div>
			</td>
	         
		</tr>
	</xsl:if>
</xsl:template>

<xsl:template match="logentry" mode="intable">
	<!-- Display a table row for the an items log -->
	<xsl:variable name="OperationalColour">
		<xsl:choose>
			<xsl:when test="@commenttype='broken'">red</xsl:when>
			<xsl:when test="@commenttype='damaged'">orange</xsl:when>
			<xsl:when test="@commenttype='niggle'">yellow</xsl:when>
			<xsl:when test="@commenttype='missing'">grey</xsl:when>
			<xsl:when test="@commenttype='fixed'">green</xsl:when>
			<xsl:when test="@commenttype='working'">green</xsl:when>
			<xsl:when test="@commenttype='decomissioned'">purple</xsl:when>
			<xsl:otherwise>transparent</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<tr>
		<!-- Display the date column -->
		<td bgcolor="{$OperationalColour}">
			<xsl:value-of select="@date" />
		</td>
		
		<!-- Display the comment author column -->
		<td align="right" bgcolor="{$OperationalColour}">
			<xsl:variable name="Author">
				<xsl:value-of select="@name" />
			</xsl:variable>
			<xsl:value-of select="$Author"/>
		</td>
		
		<!-- Display the supplier name column -->
		<td align="right" bgcolor="{$OperationalColour}">
			<xsl:if test="string-length(@supplier)&gt;0">
				<xsl:variable name="supplierid" select="@supplier"/>
				<xsl:variable name="supplierurl"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$supplierid" /></xsl:variable>
				<xsl:variable name="suppliername">
					<xsl:for-each select="document('../xml/suppliers.xml')//supplier[@id=$supplierid][last()]">
						<xsl:value-of select="@name" />
					</xsl:for-each>
				</xsl:variable>
				<a href="{$supplierurl}">
					<xsl:value-of select="$suppliername"/>
				</a>
			</xsl:if>
		</td>
		
		<!-- Display the cost column -->
		<td  align="right" bgcolor="{$OperationalColour}">
			<xsl:if test="@price&gt;0">
				<xsl:value-of select="format-number(@price,'####.00')" />
			</xsl:if>
		</td>
		
		<!-- Display the comment type column -->
		<td bgcolor="{$OperationalColour}">
			<!-- Make an unordered list of proceedures and comment type -->
	 		<!-- Put in the comment type if it is not empty -->
 			<xsl:variable name="commenttype">
				<xsl:value-of select="@commenttype" />
			</xsl:variable>
 			<xsl:if test="string-length($commenttype)&gt;0">
 				Condition: <xsl:value-of select="@commenttype" />
 			</xsl:if>
	 		<!-- Put in the proceedure if it is not empty -->
	 		<!-- Get the proceedure ID from the current node -->
 			<xsl:variable name="procedureID">
				<xsl:value-of select="@procedure" />
			</xsl:variable>
			<!-- We will look up the full proceedure name from the ID -->
			<xsl:variable name="procedureNAME">
				<xsl:value-of select="document('../xml/UBUCManual.xml')//procedure[@id=$procedureID]/@name"/>
			</xsl:variable>
 			<!-- If the string is not empty print it -->
 			<xsl:if test="string-length($procedureNAME)&gt;0">
 				Procedure: <xsl:value-of select="$procedureNAME" />
 			</xsl:if>
		</td>
		
		<!-- Display the comment column -->
		<td bgcolor="{$OperationalColour}">
			<xsl:variable name="uri">
				<xsl:value-of select="@uri" />
			</xsl:variable>
			<xsl:if test="string-length($uri)&gt;0">
				<a href="{$uri}" target="_blank">
					<xsl:value-of select="." />
				</a>
			</xsl:if>
			<xsl:if test="string-length($uri)=0">
				<xsl:value-of select="." />
			</xsl:if>
		</td>
	</tr>
</xsl:template>

   <xsl:template match="logentry" mode="suppliertable">
<!--

         Display a line of the log 

         -->
      <tr>
         <td width="12%">
               <xsl:value-of select="@name" />
            </td><td>
           
               <xsl:value-of select="@date" />
            
         </td>
       
         <td>
			<xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
		     <xsl:for-each select="document('../xml/UBUCManual.xml')//item[@id=$id] | document('../xml/UBUCManual.xml')//partbit[@id=$id] | document('../xml/UBUCManual.xml')//consumable[@id=$id] | document('../xml/UBUCManual.xml')//tool[@id=$id]">
                <xsl:apply-templates select="." mode="text"/>
			 </xsl:for-each>
         </td>
         <td>
			<xsl:variable name="proc"><xsl:value-of select="@procedure"/></xsl:variable>
		     <xsl:for-each select="//procedure[@id=$proc]">
                <xsl:apply-templates select="." mode="text"/>
			 </xsl:for-each>
         </td>

         <td width="8%" align="right">
            <xsl:if test="string-length(@price)>0">
                  <xsl:value-of select="format-number(@price,'####.00')" />
            </xsl:if>
         </td>

         <td>
            <xsl:variable name="uri">
               <xsl:value-of select="@uri" />
            </xsl:variable>

            <xsl:if test="string-length($uri)&gt;0">
               <a href="{$uri}" target="_blank">
                  <xsl:value-of select="." />
               </a>
            </xsl:if>

            <xsl:if test="string-length($uri)=0">
               <xsl:value-of select="." />
            </xsl:if>
         </td>
      </tr>
   </xsl:template>

   <xsl:template match="procedure" mode="intable">
      <xsl:param name="thisitem" />

      <xsl:variable name="procedure">
         <xsl:value-of select="@id" />
      </xsl:variable>
	  <xsl:variable name="procname"><xsl:value-of select="@name"/></xsl:variable>
      <xsl:variable name="itemname"><xsl:for-each select="//item[@id=$thisitem]"><xsl:value-of select="@name"/></xsl:for-each></xsl:variable>
      <xsl:variable name="loglink"><xsl:value-of select="$logphp"/>?id=<xsl:value-of select="$thisitem"/>&amp;proc=<xsl:value-of select="$procedure"/></xsl:variable>

                     <xsl:variable name="boxc">
                        <xsl:choose>
                           <xsl:when test="@requirement='NotAdvised'">grey</xsl:when>

                           <xsl:when test="@requirement='mandatory'">red</xsl:when>

                           <xsl:when test="@requirement='required'">orange</xsl:when>

                           <xsl:when test="@requirement='recommended'">yellow</xsl:when>

                           <xsl:otherwise>transparent</xsl:otherwise>
                        </xsl:choose>
                     </xsl:variable>


      <xsl:variable name="lastDate">
         <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$thisitem and @procedure=$procedure][last()]">
            <xsl:value-of select="@date" />
         </xsl:for-each>
      </xsl:variable>

 
         <tr>
		 <td width="5%">
		            
        
        <a href="{$loglink}" onmouseover="nhpup.popup('Click here to record completion of {$procname} on {$itemname} ');"><img src="Resources/Log.gif"/></a>
		 </td>
            <td width="5%" align="left">
               <xsl:apply-templates select="." mode="level" />
            </td>
			<td width="50%" bgcolor="{$boxc}" onmouseover="nhpup.popup('{@reason}');">
               <xsl:variable name="comment">
                  <xsl:value-of select="@name" />
               </xsl:variable>

               <xsl:variable name="link"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>

               <a href="{$link}" onmouseover="nhpup.popup('{$comment}');">
                  <xsl:value-of select="@name" />
               </a>
            </td>
         

         
            <td width="20%" onmouseover="nhpup.popup('Date {$procname} was last done on {$itemname}');">
               <xsl:value-of select="$lastDate" />
            </td>
         

        
            <td width="10%"  onmouseover="nhpup.popup('Frequency in months that {$procname} should be done on {$itemname}');">
               <xsl:value-of select="@frequency" />
            </td>
			<td width="10%" onmouseover="nhpup.popup('Likely cost of doing {$procname} on {$itemname}');">
               <xsl:value-of select="@BudgetEstimate"/>
            </td>
         </tr>

   </xsl:template>

   <xsl:template match="procedure" mode="level">
      <xsl:choose>
         <xsl:when test="@level='easy'">
            <img src="./Resources/green.jpg" onmouseover="nhpup.popup('This procedure is simple, or forms part of basic training, or the impact of it being done wrong is small so it may be done by any UBUCer');">
            </img>
         </xsl:when>

         <xsl:when test="@level='hard'">
            <img src="./Resources/yellow.jpg" onmouseover="nhpup.popup('This procedure is restricted to authorised or certified individuals');">
            </img>
         </xsl:when>

         <xsl:when test="@level='commercial'">
            <img src="./Resources/red.jpg" onmouseover="nhpup.popup('UBUC does not have the capability of doing this procedure and must pay professionals to do it');">
            </img>
         </xsl:when>

         <xsl:otherwise>?</xsl:otherwise>
      </xsl:choose>
   </xsl:template>

<!-- Template to apply for displaying a a procedure -->
<xsl:template match="procedure" mode="page">
	<xsl:variable name="procedure">
		<xsl:value-of select="@id" />
	</xsl:variable>
	
	<A NAME="{$procedure}" />
	
	<h1>
	<xsl:value-of select="@name" />
	</h1>
	<a href="{$homelink}">Back To Contents</a>
	<h2>Details</h2>
	<ul>
		<li>UBUCid=<xsl:value-of select="@id"/></li>
		<xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
		<li><a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a></li>
		<li>Category = <xsl:value-of select="@description"/></li>
		<li>Optionality = <xsl:value-of select="@requirement"/></li>
		<li>Because = <xsl:value-of select="@reason"/></li>
		<li>Estimated Cost = <xsl:value-of select="@BudgetEstimate"/></li>
		<li>Frequency in Months = <xsl:value-of select="@frequency"/></li>
		<li>Level = <xsl:value-of select="@level"/></li>
		<li>Estimated time in hrs = <xsl:value-of select="@effort"/></li>
	</ul>
	
	<xsl:variable name="loglink02"><xsl:value-of select="$logphp"/>?proc=<xsl:value-of select="$procedure"/></xsl:variable>
	<h2>Procedure Log <a href="{$loglink02}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');"><img src="Resources/Log.gif"/></a></h2>
	This procedure has been completed on the following occasions.
	<table width="100%" border="1">
		<tr>
			<th width="12%" bgcolor="lightgrey"><b>Date</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Member</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Supplier</b></th>
			<th width="8" bgcolor="lightgrey"><b>Cost</b></th>
			<th bgcolor="lightgrey"><b>Comment</b></th>
		</tr>
	
		<xsl:for-each select="document('../xml/log.xml')/logfile/logentry[string-length(@id)=0 and @procedure=$procedure]">
			<xsl:sort select="@date" order="descending" />
			<xsl:apply-templates select="." mode="intable" />
		</xsl:for-each>
	</table>
	
	<h2>Itemlist</h2>
	<p>This procedure applies to the following items:</p>
	<xsl:apply-templates select="." mode="itemlist" />
	
	<h2>Required Items</h2>
	The following items are referenced in the procedure and will be required to complete it.
	<table width="100%" border="1">
		<tr>        
			<th>Item</th>
			<th>Attributes</th>
			<th>In Stores?</th>
			<th>Replacements to buy</th>
		</tr>
		
		<!-- Add Tools, Parts and Attributes -->
		<xsl:variable name="RequiredItems">
			<xsl:for-each select="descendant::toolref|descendant::partref[string-length(@number)>0]|descendant::consumableref">,<xsl:value-of select="@id"/>,</xsl:for-each>
		</xsl:variable>
		
		<xsl:for-each select="//tool|//consumable|//partbit">
			<xsl:sort select="@name"/>
			<xsl:variable name="thingid"><xsl:value-of select="@id"/></xsl:variable>
			<xsl:variable name="testid">,<xsl:value-of select="@id"/>,</xsl:variable>
			<xsl:if test="contains($RequiredItems,$testid)">
				<tr>
					<td>
						<xsl:apply-templates select="." mode="text"/>
					</td>
					<td>
						<ul>
							<xsl:for-each select="(@*)">
								<xsl:sort select="name()" />
								<xsl:if test="name() != 'id' and name() != 'name' and name() != 'image' and name() != 'store'">
									<li>
										<xsl:value-of select="name()" />
										=
										<xsl:value-of select="." />
										</li>
								</xsl:if>
							</xsl:for-each>
						</ul>
						<!--table width="100%">
						<xsl:variable name="thisitem08">
						<xsl:value-of select="@id" />
						</xsl:variable>
						
						<TDW xsl:for-each select="document('../xml/log.xml')/logfile/logentry[@id=$thisitem08]">
						<xsl:sort select="@date" order="descending" />
						
						<xsl:apply-templates select="." mode="intable" />
						</xsl:for-each>
						</table-->
					</td>
					<td>
						<xsl:value-of select="@store" />
					</td>
					<td>
						<xsl:variable name="needed"><xsl:value-of select="//procedure[@id=$procedure]/descendant::partref[@id=$thingid]/@number"/></xsl:variable>
						<xsl:if test="$needed=0">
							If required
						</xsl:if>
						<xsl:if test="$needed>0">
							<xsl:value-of select="$needed"/>
						</xsl:if>        
						<!--document('../xml/log.xml')//logentry[@supplier=$suppliername] -->
					</td>
				</tr>
			</xsl:if>
		</xsl:for-each>
	</table>
	
	<!-- Add Step Instructions -->
	<xsl:for-each select="descendant::step">
		<h3>
		<xsl:value-of select="@name" />
		</h3>
	
		<xsl:apply-templates select="." mode="text" />
	</xsl:for-each>
</xsl:template>

   <xsl:template match="refid" mode="text">
      <xsl:variable name="id">
         <xsl:value-of select="@id" />
      </xsl:variable>
	  <i>
      <xsl:for-each select="//ref[@id=$id]">
	      <xsl:variable name="link">
		  href=
		            <xsl:if test="string-length(@uri)=0">
              "<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" />"
          </xsl:if>
		  <xsl:if test="string-length(@uri)>0">
              "<xsl:value-of select="@uri"/>"
          </xsl:if>
		  <xsl:if test="string-length(@img)>0">
              "<xsl:value-of select="@img"/>"
          </xsl:if>

		  </xsl:variable>

          <xsl:variable name="fred">&lt;a <xsl:value-of select="$link" />  onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />  &amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; 
          <xsl:value-of select="@name" />
          &lt;/a&gt;</xsl:variable>

      
         <xsl:value-of select="$fred" disable-output-escaping="yes" />


      </xsl:for-each>

      <sub>( 
      <xsl:for-each select="(@*)">
         <xsl:sort select="name()" />

         <xsl:if test="name() != 'id'">
         <xsl:value-of select="name()" />

         =
         <xsl:value-of select="." />

         ,</xsl:if>
      </xsl:for-each>

      )</sub>
	  </i>
   </xsl:template>

   <xsl:template match="partref|consumableref|toolref|procref" mode="text">
      <xsl:variable name="id">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <xsl:variable name="link">href="<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" />"</xsl:variable>

      <xsl:variable name="fred">&lt;a <xsl:value-of select="$link" disable-output-escaping="yes" />  onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />  &amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; 
      <xsl:value-of select="//partbit[@id=$id]/@name" />

      <xsl:value-of select="//consumable[@id=$id]/@name" />

      <xsl:value-of select="//tool[@id=$id]/@name" />

	  <xsl:value-of select="//procedure[@id=$id]/@name" />	   

      &lt;/a&gt;</xsl:variable>

      <i>
         <xsl:value-of select="$fred" disable-output-escaping="yes" />

         <xsl:apply-templates select="./refid" mode="text" />
      </i>
   </xsl:template>

   <xsl:template match="supplier|item|procedure|partbit|tool|consumable|ref" mode="text">
      <xsl:variable name="id">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <xsl:variable name="link">href="<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" />"</xsl:variable>

      <xsl:variable name="fred">&lt;a <xsl:value-of select="$link" disable-output-escaping="yes" />  onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />  &amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; 
       <xsl:value-of select="@name" />
      &lt;/a&gt;</xsl:variable>

      <i>
         <xsl:value-of select="$fred" disable-output-escaping="yes" />
      </i>
   </xsl:template>


   <xsl:template match="ref" mode="text">
      <xsl:variable name="img">
         <xsl:value-of select="@img" />
      </xsl:variable>

      <xsl:variable name="uri">
         <xsl:value-of select="@uri" />
      </xsl:variable>

      <xsl:if test="string-length($uri)&gt;0">
         <a href="{$uri}" target="_blank">
         <xsl:value-of select="@id" />

         - 
         <xsl:value-of select="@name" />

         <xsl:value-of select="." />
         </a>
      </xsl:if>

      <xsl:if test="string-length($uri)=0">
      <xsl:value-of select="@id" />

      - 
      <xsl:value-of select="@name" />

      <xsl:value-of select="." />
      </xsl:if>
   </xsl:template>

   <xsl:template match="partbit|consumable|tool" mode="table">
      <xsl:variable name="thisitem01">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <tr>
         <td>
            <a NAME="{$thisitem01}" />
			<xsl:apply-templates select="." mode="text"/>

         </td>

         <td>
            <table>
               <xsl:for-each select="(@*)">
                  <xsl:sort select="name()" />

                  <xsl:if test="name() != 'name' and name() != 'store'">
                     <tr>
                        <td width="40%">
                           <xsl:value-of select="name()" />
                        </td>

                        <td>
                           <xsl:value-of select="." />
                        </td>
                     </tr>
                  </xsl:if>
               </xsl:for-each>
            </table>
         </td>

         <td>
            <ul>
			<xsl:variable name="referencedProcs">
               <xsl:for-each select="//toolref[@id=$thisitem01]|//partref[@id=$thisitem01]|//consumableref[@id=$thisitem01]">

                  <xsl:for-each select="ancestor::procedure">,<xsl:value-of select="@id"/>,</xsl:for-each>
               </xsl:for-each>
		   </xsl:variable>

		   <xsl:for-each select="//procedure"> 
		   <xsl:sort select="@name" order="ascending"/>
		       <xsl:variable name="idcheck">,<xsl:value-of select="@id"/>,</xsl:variable>
               <xsl:if test="contains($referencedProcs,$idcheck)">
                   <xsl:variable name="proclink"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>
                  
                     <li>
                       <a href="{$proclink}">
                           <xsl:value-of select="@name" />
                        </a>
                     </li>
					 </xsl:if>
               </xsl:for-each>
            </ul>
         </td>

         <td>
            <table width="100%">
               <xsl:for-each select="document('../xml/log.xml')/logfile/logentry[@id=$thisitem01]">
                  <xsl:sort select="@date" order="descending" />

                  <xsl:apply-templates select="." mode="intable" />
               </xsl:for-each>
            </table>
         </td>
      </tr>
   </xsl:template>

   <xsl:template match="partbit|consumable|tool|ref" mode="hidden">
      <xsl:variable name="parturl">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <div style="display:none;" id="{$parturl}">
         <table width="200" border="1">
            <xsl:if test="@image">
               <xsl:variable name="img">
                  <xsl:value-of select="@image" />
               </xsl:variable>

               <img width="150" src="{$img}" />
            </xsl:if>

            <tbody>
               <xsl:for-each select="(@*)">
                  <xsl:sort select="name()" />
                     <xsl:if test="name() != 'name' and name() != 'uri' and name() != 'image'">
                     <tr>
                        <td>
                           <xsl:value-of select="name()" />
                        </td>

                        <td>
                           <xsl:value-of select="." />
                        </td>
                     </tr>
					 </xsl:if>
               </xsl:for-each>

            </tbody>
         </table>
      </div>
   </xsl:template>

   <xsl:template match="item" mode="hidden">
      <xsl:variable name="parturl">
         <xsl:value-of select="@id" />
      </xsl:variable>

      <div style="display:none;" id="{$parturl}">
         <table width="200" border="1">
            <xsl:if test="@image">
               <xsl:variable name="img">
                  <xsl:value-of select="@image" />
               </xsl:variable>

               <img width="150" src="{$img}" />
            </xsl:if>

            <tbody>
               <xsl:for-each select="(@*)">
                  <xsl:sort select="name()" />
	              <xsl:if test="name() != 'name' and name() != 'image'">
 
                     <tr>
                        <td>
                           <xsl:value-of select="name()" />
                        </td>

                        <td>
                           <xsl:value-of select="." />
                        </td>
                     </tr>
                  </xsl:if>
               </xsl:for-each>
            </tbody>
         </table>
      </div>
   </xsl:template>

<xsl:template match="supplier" mode="page">
	<xsl:variable name="suppliername">
		<xsl:value-of select="@id" />
	</xsl:variable>
	
	<A NAME="{$suppliername}" />
	
	<h1><xsl:value-of select="@name" /></h1>
	<xsl:variable name="editLink">http://ubuc.org/maintenance/editSupplier.php?id=<xsl:value-of select='@id'/></xsl:variable>
    <a href="{$editLink}">Edit this supplier</a>
	
	<ul>
		<li>UBUCid=<xsl:value-of select="@id"/></li>
		<xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
		<li><a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a></li>
		<li>Tel= <xsl:value-of select="@tel" /></li>
		<li>Email= 	<xsl:value-of select="@email" /></li>
		<xsl:variable name="suri"><xsl:value-of select="@uri"/></xsl:variable>
		<li><a href="{$suri}">website</a></li>
	</ul>
	
	<xsl:variable name="loglink04"><xsl:value-of select="$logphp"/>?supplier=<xsl:value-of select="@id"/></xsl:variable>
	<h2>Supplier Log <a href="{$loglink04}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');"><img src="Resources/Log.gif"/></a></h2>
	
	<table>
		<tr>
			<th>Name</th>
			<th>Date</th>
			<th>Object</th>
			<th>Procedure</th>
			<th>Price</th>
			<th>Comment</th>
		</tr>
		<xsl:for-each select="document('../xml/log.xml')//logentry[@supplier=$suppliername]">
			<xsl:sort select="@date" order="descending"/>
			<xsl:apply-templates select="." mode="suppliertable" />
		</xsl:for-each>
	</table>
</xsl:template>

	<!-- Template for printing out all the items for which a particular proceedure is upcoming -->
	<xsl:template match="procedure" mode="itemlist">
      	<xsl:variable name="procedurename"><xsl:value-of select="@id" /></xsl:variable>

      	<xsl:for-each select="../item|../itemgroup/item">
         	<xsl:variable name="thisvar"><xsl:value-of select="@id" /></xsl:variable>

			<!-- Setup a variable with the date the proceedure was last complete. Not used after update to display item name rather than date. -->
			<xsl:variable name="lastdone">
			   <xsl:for-each select="document('../xml/log.xml')//logentry[@procedure=$procedurename and @id=$thisvar][last()]">
			      <xsl:value-of select="@date" />
			   </xsl:for-each>
			</xsl:variable>

         	<xsl:variable name="OperationalState">
            	<xsl:value-of select="@status"/>
         	</xsl:variable>

         	<xsl:variable name="OperationalLink">
            	<xsl:choose>
               		<xsl:when test="$OperationalState='broken'">brokenlink</xsl:when>
               		<xsl:when test="$OperationalState='damaged'">damagedlink</xsl:when>
               		<xsl:when test="$OperationalState='niggle'">nigglelink</xsl:when>
               		<xsl:when test="$OperationalState='missing'">lostlink</xsl:when>
               		<xsl:otherwise>link</xsl:otherwise>
            	</xsl:choose>
         	</xsl:variable>

         	<xsl:if test="$OperationalState!='decomissioned'">
         		<xsl:variable name="itemnamea"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>
         		<xsl:variable name="fred">&lt;a href="<xsl:value-of select="$itemnamea" />" class="<xsl:value-of select="$OperationalLink" />" onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />&amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; (<xsl:value-of select="@id" />)&lt;/a&gt;</xsl:variable>
         		<xsl:value-of select="$fred" disable-output-escaping="yes" />,
         	</xsl:if>
		</xsl:for-each>
	</xsl:template>

   <xsl:template match="partbit|consumable|tool|item" mode="name">
      <xsl:value-of select="@name" />
   </xsl:template>


  <xsl:template match="partbit|consumable|tool|ref" mode="page">
  <xsl:variable name="thisid"><xsl:value-of select="@id"/></xsl:variable>
   <h1><xsl:value-of select="@name"/></h1>
   <ul>
   <li>UBUCid=<xsl:value-of select="@id"/></li>
   <xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
   <li><a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a></li>
   </ul>

   <xsl:if test="string-length(@image)>0">
   <h2>Image</h2>

   <img src="{@image}"/>
   </xsl:if>
   <h2>Other Attributes</h2>
   <ul>
                 <xsl:for-each select="(@*)">
                     <xsl:sort select="name()" />

                     <xsl:if test="name() != 'id' and name() != 'name' and name() != 'image'">
                        <li>
                              <xsl:value-of select="name()" />
                           =
                              <xsl:value-of select="." />
                         </li>
                     </xsl:if>
                  </xsl:for-each>
 </ul>
    <h2>Procedures</h2>
   <xsl:variable name="UsingProcedures">
   <xsl:for-each select="//*[@id=$thisid]">
      <xsl:for-each select="ancestor::procedure">,<xsl:value-of select="@id"/>,</xsl:for-each> 
   </xsl:for-each>
   </xsl:variable>
   <ul>
   <xsl:for-each select="//procedure">
      <xsl:if test="contains($UsingProcedures,concat(',',@id,','))">
          <li><xsl:apply-templates select="." mode="text"/></li>
	  </xsl:if>
   </xsl:for-each>
   </ul>
<h2>References</h2>
<ul>
<xsl:for-each select="ancestor-or-self::refid|child::refid">
<li><xsl:apply-templates select="." mode="text" /></li>
</xsl:for-each>
</ul>
       <xsl:variable name="loglink03"><xsl:value-of select="$logphp"/>?id=<xsl:value-of select="@id"/></xsl:variable>
      <h2><xsl:value-of select="name()"/> Log <a href="{$loglink03}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');"><img src="Resources/Log.gif"/></a></h2>


                  <table width="100%">
				  				  <tr>
                  <th width="12%" bgcolor="lightgrey"><b>Date</b></th>
				  <th width="12%" bgcolor="lightgrey"><b>Member</b></th>
				  <th width="12%" bgcolor="lightgrey"><b>Supplier</b></th>
				  <th width="8" bgcolor="lightgrey"><b>Cost</b></th>
				  <th bgcolor="lightgrey"><b>Comment</b></th>
				  </tr>

                     <xsl:variable name="thisitem06"><xsl:value-of select="@id" /></xsl:variable>

                     <xsl:for-each select="document('../xml/log.xml')/logfile/logentry[@id=$thisitem06]">
                        <xsl:sort select="@date" order="descending" />

                        <xsl:apply-templates select="." mode="intable" />
                     </xsl:for-each>
                  </table>

   </xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\FirstPass.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->