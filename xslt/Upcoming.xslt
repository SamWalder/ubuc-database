<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:param name="CurrentDate"/>
<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>
<xsl:variable name="now" select="(12*substring($CurrentDate,1,4))+substring($CurrentDate,6,2)-1"/>

<xsl:template match="/">
<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61486837-1', 'auto');
  ga('send', 'pageview');

</script>
<style>br{mso-data-placement:same-cell;}
</style>
</head>
<body>
<table border="1" width="100%">
<tr>
<th>Date</th>
<th>ID</th>
<th>Name</th>
<th>Type</th>
<th>Category</th>
<th>Status</th>
<th>Procedure</th>
<th>Frequency</th>
<th>LastDone</th>
<th>Done By</th>
<th>Estimated Cost</th>
<th>Estimated Effort (hrs)</th>
<th>Responsible Officer</th>
<th>Optionality</th>
<th>Level</th>
</tr>

<xsl:call-template name="repeatable" />

</table>
</body>
</html>	
</xsl:template>


<xsl:template name="repeatable">
    <xsl:param name="index" select="0" />
    <xsl:param name="total" select="12" />



<xsl:variable name="PredictionTime" select="$now+$index"/>


<xsl:for-each select="//item">
<xsl:if test="@status!='decomissioned'">
<xsl:variable name="itemid" select="@id"/>
<xsl:variable name="itemName" select="@name"/>
<xsl:variable name="itemStatus" select="@status"/>
<xsl:variable name="itemType"><xsl:value-of select="ancestor::itemgroup/@name"/></xsl:variable>
<xsl:variable name="itemCategory"><xsl:value-of select="ancestor::collection[@name][1]/@name" /></xsl:variable>

<xsl:variable name="officer" select="ancestor::collection[@officer][1]/@officer" />
<xsl:for-each select="ancestor::itemgroup/procedure|ancestor::collection/procedure">
<xsl:if test="@frequency &gt; 0">
<xsl:variable name="procid" select="@id"/>
      <xsl:variable name="lastDate">
         <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
            <xsl:value-of select="@date" />
         </xsl:for-each>
      </xsl:variable>
	
<xsl:variable name="counter">
<xsl:if test="$lastDate != ''">
<xsl:variable name="year">
<xsl:value-of select="substring($lastDate,1,4)"/>
</xsl:variable>
<xsl:variable name="month">
<xsl:value-of select="substring($lastDate,6,2)"/>
</xsl:variable>
<xsl:variable name="lastactual" select="(12*$year)+$month -1"/>
<xsl:if test="not($now &gt; ($lastactual+@frequency))" >
 <xsl:value-of select="$lastactual"/>
</xsl:if>
<xsl:if test="$now &gt; ($lastactual+@frequency)">
<xsl:value-of select="$now - @frequency"/>
</xsl:if>
</xsl:if>
<xsl:if test="$lastDate = ''">
<xsl:value-of select="$now - @frequency"/>
</xsl:if>
</xsl:variable>



<xsl:if test="(($PredictionTime - $counter) mod @frequency)=0 and not($PredictionTime=$counter)">
<tr>
<td><xsl:value-of select="1+($PredictionTime mod 12)"/>-<xsl:value-of select="floor($PredictionTime div 12)"/></td>
<td>
 <xsl:variable name="generalid"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$itemid"/></xsl:variable>
 <a href="{$generalid}"><xsl:value-of select="$itemid"/></a></td>
<td><xsl:value-of select="$itemName"/></td>

<td><xsl:value-of select="$itemType"/></td>
<td><xsl:value-of select="$itemCategory" /> </td>
<td><xsl:value-of select="$itemStatus"/></td>
<td>
<xsl:variable name="proclink"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$procid" /></xsl:variable>
<a href="{$proclink}"><xsl:value-of select="@name" /></a></td>
<td><xsl:value-of select="@frequency"/></td>
<td>


<xsl:value-of select="$lastDate"/>

</td>
<td>
<xsl:if test="$lastDate != ''">

         <xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
            <xsl:value-of select="@name" />
         </xsl:for-each>

</xsl:if>
</td>
<td><xsl:value-of select="@BudgetEstimate"/></td>
<td><xsl:value-of select="@effort"/></td>
<td><xsl:value-of select="$officer"/></td>
<td><xsl:value-of select="@requirement"/></td>
<td><xsl:value-of select="@level"/></td>

</tr>
</xsl:if>
</xsl:if>
</xsl:for-each>
</xsl:if>
</xsl:for-each>

    <xsl:if test="not($index = $total)">
        <xsl:call-template name="repeatable">
            <xsl:with-param name="index" select="$index + 1" />
        </xsl:call-template>
    </xsl:if>
</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\FirstPass.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->