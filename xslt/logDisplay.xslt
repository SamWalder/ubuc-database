<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:variable name="constlink">http://www.ubuc.org/index.php?page=club-constitution</xsl:variable>
	<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>

	<xsl:template match="/">
		<html>
			<head>
				<!-- 
            J Query script declarations.  Thes enable java script functionality like pop up boxes on mouse over
             -->
				<script src="http://code.jquery.com/jquery-1.10.1.min.js">
				</script>

				<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js">
				</script>

				<script type="text/javascript" src="./javascript/nhpup_1.1.js">
				</script>

				<!-- 
            Style section contains CSS layout instructions
            -->
				<style>

					<!--
              Mouse over pop-up box formatting
              -->#pup {position:absolute;
                          z-index:200; /* aaaalways on top*/
                          padding: 3px;
                          margin-left: 10px;
                          margin-top: 5px;
                          width: 250px;
                          border: 1px solid black;
                          background-color: #777;
                          color: white;
                          font-size: 0.95em;
                          }</style>
				<link rel="stylesheet" type="text/css" href="./css/UBUCManual.css"></link>

				<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61486837-1', 'auto');
  ga('send', 'pageview');</script>
			</head>

			<!--
        HTML body section displaying the UBUC manual
        -->
			<body>

				<h1>UBUC Maintenance Documentation</h1>
				<p>
					<a href="{$constlink}">This club exists to provide for its members  facilities, opportunity and training for underwater exploration, science, and sport.</a>
				</p>
				<p>
					<a href="{$homelink}">Back To Contents</a>
				</p>

				<h1>Maintenance Log</h1>
				<table border="1" width="100%">
					<!--tr>
		<th width="10%">State</th><th width="45%">Costed Items</th><th width="45%">Uncosted Items</th>
		</tr-->
					<tr>
						<th>Date</th>
						<th>Responsible Officer</th>
						<th>Comment Type</th>
						<th>Author</th>
						<th>Item id</th>
						<th>Item Name</th>
						<th>Procedure</th>
						<th>Supplier</th>
						<th>Price</th>
						<th>Text</th>
					</tr>

					<xsl:for-each select="//logentry">
						<xsl:sort select="@date" order="descending"/>
						<xsl:variable name="id" select="@id"/>
						<xsl:variable name="procid" select="@procedure"/>
						<tr>
							<td>
								<xsl:value-of select="@date"/>
							</td>
							<td>
								<xsl:variable name="Resp">
									<xsl:for-each select="(document('../xml/UBUCManual.xml')//item[@id=$id]|document('../xml/UBUCManual.xml')//toolref[@id=$id]|document('../xml/UBUCManual.xml')//partref[@id=$id]|document('../xml/UBUCManual.xml')//consumableref[@id=$id]|document('../xml/UBUCManual.xml')//procedure[@id=$procid])[1]">
										<xsl:value-of select="ancestor::collection[@officer][1]/@officer"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="$Resp"/>
							</td>

							<td>
								<xsl:value-of select="@commenttype"/>
							</td>
							<td>
								<xsl:value-of select="@name"/>
							</td>
							<xsl:variable name="thisid" select="@id"/>
							<td>

								<xsl:variable name="idlink">
									<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>
								<a href="{$idlink}">
									<xsl:value-of select="$thisid"/>
								</a>
							</td>
							<td>
								<xsl:value-of select="document('../xml/UBUCManual.xml')//item[@id=$thisid][1]/@name"/>
							</td>
							
							<td>

								<!--xsl:variable name="procid" select="@procedure"/-->
								<xsl:variable name="procname">
									<xsl:value-of select="document('../xml/UBUCManual.xml')//procedure[@id=$procid][1]/@name"/>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$procname=''">
										<xsl:value-of select="$procid"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="idlink1">
											<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$procid"/></xsl:variable>
										<a href="{$idlink1}">
											<xsl:value-of select="$procname"/>
										</a>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td>
								<xsl:variable name="supid" select="@supplier"/>
								<xsl:variable name="supname">
									<xsl:value-of select="document('../xml/suppliers.xml')//supplier[@id=$supid][1]/@name"/>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$supname=''">
										<xsl:value-of select="$supid"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="idlink2">
											<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$supid"/></xsl:variable>
										<a href="{$idlink2}">
											<xsl:value-of select="$supname"/>
										</a>
									</xsl:otherwise>
								</xsl:choose>
								
							</td>
							<td>
								<xsl:value-of select="@price"/>
							</td>
							<td>
								<xsl:value-of select="."/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\log.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->