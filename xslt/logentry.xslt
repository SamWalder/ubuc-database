<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="id"/>
	<xsl:param name="proc"/>
	<xsl:param name="supplier"/>
	<xsl:param name="CurrentDate"/>

	<xsl:variable name="item"/>
	<xsl:variable name="now" select="(12*substring($CurrentDate,1,4))+substring($CurrentDate,6,2)-1"/>


	<xsl:template match="/">
		<html>
			<head>
				<!-- J Query script declarations.  These enable java script functionality like pop up boxes on mouse over -->
				<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
				<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
				<script type="text/javascript" src="./javascript/nhpup_1.1.js"></script>

				<!-- Style section contains CSS layout instructions-->
				<style>
					<!--Mouse over pop-up box formatting-->
					#pup {position:absolute;
                          z-index:200; /* aaaalways on top*/
                          padding: 3px;
                          margin-left: 10px;
                          margin-top: 5px;
                          width: 250px;
                          border: 1px solid black;
                          background-color: #777;
                          color: white;
                          font-size: 0.95em;
                          }
				</style>
				
				<link rel="stylesheet" type="text/css" href="css/UBUCManual.css"/>
                <script src='https://www.google.com/recaptcha/api.js'></script>
			</head>

			<body>
                <xsl:variable name="CheckId">
					<xsl:if test="$id='' and $proc!=''">
						<xsl:value-of select="$proc"/>
					</xsl:if>
					<xsl:if test="$id='' and ($proc='' and $supplier!='')">
						<xsl:value-of select="$supplier"/>
					</xsl:if>
					<xsl:if test="$id!=''">
						<xsl:value-of select="$id"/>
					</xsl:if>
				</xsl:variable>
				
				<xsl:for-each select="//item[@id=$CheckId] | //procedure[@id=$CheckId] | //partbit[@id=$CheckId] | //consumable[@id=$CheckId] | //tool[@id=$CheckId] | //ref[@id=$CheckId] | //supplier[@id=$CheckId]">
					<xsl:variable name="idcheck" select="name()"/>
  			

					<xsl:variable name="type">
						<xsl:choose>
							<xsl:when test="string-length($id)&gt;0 and string-length($proc)=0">type01</xsl:when>
							<xsl:when test="string-length($id)&gt;0 and string-length($proc)&gt;0">type03</xsl:when>
							<xsl:when test="string-length($id)=0 and string-length($proc)&gt;0">type04</xsl:when>
							<xsl:when test="string-length($id)=0 and string-length($supplier)&gt;0">type05</xsl:when>
						</xsl:choose>
					</xsl:variable>




					<form method="post" action="Log.php" enctype="multipart/form-data">

						<!-- Display a header for the form -->
						<h1>
							<xsl:choose>
								<xsl:when test="$type='type01' and $idcheck='item'">Record your information about this <xsl:value-of select="$idcheck"/></xsl:when>
								<xsl:when test="$type='type01' and $idcheck!='item'">Record your information relating to <xsl:value-of select="$idcheck"/></xsl:when>
								<xsl:when test="$type='type01' and $idcheck!='item'">Record your information relating to <xsl:value-of select="$idcheck"/></xsl:when>
								<xsl:when test="$type='type03'">Record that the procedure is completed on this item</xsl:when>
								<xsl:when test="$type='type04'">Record your information about this procedure</xsl:when>
								<xsl:when test="$type='type05'">Record your information about this supplier</xsl:when>
								<xsl:otherwise>Log entry is not understood</xsl:otherwise>
							</xsl:choose>
						</h1>

						<!-- Name input field -->
						<h2>Please provide your name</h2>
						<label for="name">Your Name:</label>
						<input type="Text" name="name" autocomplete="on"/>

						<!-- Date input field -->
						<h2>What date would you like this comment to be recorded with?</h2>
						<label for="date">Comment Date:</label>
						<input type="date" name="date" autocomplete="on"/>

						<!-- Area for the textual comment they will write -->
						<h2>Text Comment</h2>
						<label for="Comment">Textual comment:</label>
						<textarea id="Comment" name="comment" autocomplete="off"/>

						<xsl:choose>
							<xsl:when test="$idcheck = 'item'">

								<h2>Input Comment Type</h2>
								<select name="status" title="Select status of item">
									<option value="none">none - Comment is not relevant to items status</option>
									
									<!-- It is only possible to select working if no procedures that are:
										mandatory,
										required,
										or recommended, are out of date-->
									<xsl:variable name="itemid" select="@id"/>
									
									<!--Decide if we are allowed to record as working-->
									<xsl:variable name="AllowedWorking">
										<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and ( @requirement = 'recommended' or @requirement = 'required' or @requirement = 'mandatory' ) and @id != $proc ]|ancestor::collection/procedure[@frequency &gt; 0 and ( @requirement = 'recommended' or @requirement = 'required' or @requirement = 'mandatory' ) and @id != $proc ]">
											<xsl:variable name="procid" select="@id"/>
											<xsl:variable name="lastDate">
												<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procid][last()]">
													<xsl:value-of select="@date"/>
												</xsl:for-each>
											</xsl:variable>


											<xsl:if test="$lastDate != ''">
												<xsl:variable name="year">
													<xsl:value-of select="substring($lastDate,1,4)"/>
												</xsl:variable>
												<xsl:variable name="month">
													<xsl:value-of select="substring($lastDate,6,2)"/>
												</xsl:variable>
												<xsl:variable name="lastactual" select="(12*$year)+$month -1"/>
												<xsl:if test="$now &gt; ($lastactual+@frequency)">NotAllowed</xsl:if>
											</xsl:if>
											<xsl:if test="$lastDate = ''">NotAllowed</xsl:if>
										</xsl:for-each>
									</xsl:variable>

									<!--Decide if we are allowed to record as a niggle-->
									<xsl:variable name="AllowedNiggle">
										<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and (  @requirement = 'required' or @requirement = 'mandatory' ) and @id != $proc ]|ancestor::collection/procedure[@frequency &gt; 0 and ( @requirement = 'required' or @requirement = 'mandatory' ) and @id != $proc ]">
											<xsl:variable name="procidn" select="@id"/>
											<xsl:variable name="lastDaten">
												<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procidn][last()]">
													<xsl:value-of select="@date"/>
												</xsl:for-each>
											</xsl:variable>


											<xsl:if test="$lastDaten != ''">
												<xsl:variable name="yearn">
													<xsl:value-of select="substring($lastDaten,1,4)"/>
												</xsl:variable>
												<xsl:variable name="monthn">
													<xsl:value-of select="substring($lastDaten,6,2)"/>
												</xsl:variable>
												<xsl:variable name="lastactualn" select="(12*$yearn)+$monthn -1"/>
												<xsl:if test="$now &gt; ($lastactualn+@frequency)">NotAllowed</xsl:if>
											</xsl:if>
											<xsl:if test="$lastDaten = ''">NotAllowed</xsl:if>
										</xsl:for-each>
									</xsl:variable>
									
									<!--Decide if we are allowed to record as damaged-->
									<xsl:variable name="AllowedDamaged">
										<xsl:for-each select="ancestor::itemgroup/procedure[@frequency &gt; 0 and (  @requirement = 'mandatory' ) and @id != $proc ]|ancestor::collection/procedure[@frequency &gt; 0 and (  @requirement = 'mandatory' ) and @id != $proc ]">
											<xsl:variable name="procidn" select="@id"/>
											<xsl:variable name="lastDaten">
												<xsl:for-each select="document('../xml/log.xml')//logentry[@id=$itemid and @procedure=$procidn][last()]">
													<xsl:value-of select="@date"/>
												</xsl:for-each>
											</xsl:variable>


											<xsl:if test="$lastDaten != ''">
												<xsl:variable name="yearn">
													<xsl:value-of select="substring($lastDaten,1,4)"/>
												</xsl:variable>
												<xsl:variable name="monthn">
													<xsl:value-of select="substring($lastDaten,6,2)"/>
												</xsl:variable>
												<xsl:variable name="lastactualn" select="(12*$yearn)+$monthn -1"/>
												<xsl:if test="$now &gt; ($lastactualn+@frequency)">NotAllowed</xsl:if>
											</xsl:if>
											<xsl:if test="$lastDaten = ''">NotAllowed</xsl:if>
										</xsl:for-each>
									</xsl:variable>

									<!--Add the options to the list-->
									<xsl:if test="$AllowedWorking = ''">
										<option value="working">Working - Item is working</option>
									</xsl:if>
									<xsl:if test="$AllowedNiggle = ''">
										<option value="niggle">Niggle - Item has an issue that does not impact its use</option>
									</xsl:if>
									<xsl:if test="$AllowedDamaged = ''">
										<option value="damaged">Damaged - Item has an issue that means it must be used with care or under limited circumstances</option>
									</xsl:if>
									<option value="decomissioned">Decommissioned - Item has been decommissioned or sold</option>
									<option value="broken">Broken - Item must not be used</option>
									<option value="missing">Missing - Item is lost</option>
									<option value="image">Image - Load an image associated with this item</option>
									<option value="valuation">Valuation - Provide an estimate for current sale price achievable for this item</option>
									<option value="quote">Quote - Provide an estimate of purchasing a new replacement for this item</option>
								</select>
							</xsl:when>
							<xsl:when test="$type='type04'"><!--Comment is on a procedure-->
								<h2>Input Comment Type</h2>
								<select name="status" title="Select status of item">
									<option value="none">none - Comment is a general comment about this procedure</option>
									<option value="estimate">estimate - Provide an estimate of performing this procedure on one item</option>
								</select>
							</xsl:when>
							<xsl:when test="$idcheck = 'ref'">
								<input type="hidden" name="status" value="none"/>
							</xsl:when>
							<xsl:when test="$type='type05'">
								<select name="status" title="Select status of item">
									<option value="none">none - Comment is a general comment about thing</option>
									<option value="purchase">purchase - Price is the money exchanged with supplier</option>
								</select>
							</xsl:when>
							<xsl:otherwise>
								<h2>Input Comment Type</h2>
								<select name="status" title="Select status of item">
									<option value="none">none - Comment is a general comment about thing</option>
									<option value="quote">quote - Price is the estimated cost of one unit</option>
									<option value="image">Image - Load an image associated with this thing</option>
								</select>
							</xsl:otherwise>
						</xsl:choose>
						
						<!-- Cost Input Field -->
						<h2>Please enter the cost</h2>
						<input type="Number" name="price" title="Input Price"/>

						<!-- Supplier Input Field -->
						<xsl:if test="$type != 'type05'">
							<h2>Please provide the supplier</h2>
							<select name="supplier" title="Choose supplier">
								<option value="none">none</option>
								<xsl:for-each select="document('../xml/suppliers.xml')//supplier">
									<xsl:sort select="@name"/>
									<xsl:if test="string-length(@closed)=0">
										<xsl:variable name="idb">
											<xsl:value-of select="@id"/>
										</xsl:variable>
										<option value="{$idb}">
											<xsl:value-of select="@name"/>
										</option>
									</xsl:if>
								</xsl:for-each>
							</select>
						</xsl:if>
						<xsl:if test="$type = 'type05'">
							<input type="hidden" name="supplier" value="{$supplier}"/>
						</xsl:if>

						<!-- Additional information and file upload fields -->
						<h2>Additional information</h2>
						<p>It is really useful to be add extra information. Here you can either upload a file to associate with this comment, select an existing file to associate, or add a link to a website.</p>
						<ul>
							<li>Link this comment to previously loaded file
								<select name="resource" onmouseover="nhpup.popup('Link this comment to a previously loaded file');">
									<option value="none">none</option>
									<xsl:for-each select="document('../xml/LogResources.xml')//resource">
										<xsl:sort select="@loaded" data-type="number" order="descending"/>
										<xsl:variable name="path">
											<xsl:value-of select="@path"/>
										</xsl:variable>
										<option value="{$path}">
											<xsl:value-of select="$path"/>
										</option>
									</xsl:for-each>
								</select>
							</li>
							<li>
								<!-- Upload a new file here -->
								Upload a new file. The following extensions are accepted: "gif", "jpg", "jpeg", "png", "doc", "docx", "pdf"
								<ul>
									<li>
										<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
										<label for="userUploadedFile">Filename:</label>
										<input type="file" name="userUploadedFile" id="file"/>
									</li>
									<li>
										<label for="dname">Descriptive filename:</label>
										<input type="Text" name="dname" autocomplete="off"/>
									</li>
								</ul>
							</li>
							<li>
								<li>Input the url of a useful web page <input type="Text" name="uri" autocomplete="on" onmouseover="nhpup.popup('This will be ignored if a file is selected above');"/></li>
							</li>
						</ul>
						<input type="hidden" name="id" value="{$id}"/>
						<input type="hidden" name="procedure" value="{$proc}"/>
                        <h2>Confirm you are not a robot and submit the information</h2>
						<div class="g-recaptcha" data-sitekey="6Lf7_Q8TAAAAAMlAwFUqeIM9tWxlG819RwvFOam8"></div>
						<br/>
						<input type="submit" value="Submit"/>
					</form>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\UBUCManual.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->