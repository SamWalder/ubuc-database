<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Procedure.xlst
	This file makes the template for displaying a procedure.  -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:err="http://www.w3.org/2005/xqt-errors" 
 xmlns:itemlookup="http://ubuc.org/maintenance/item"   exclude-result-prefixes="xs xdt err fn" xmlns:php="http://php.net/xsl" xsl:extension-element-prefixes="php">
	<xsl:output method="html" indent="yes" />

	<xsl:param name="id"/>
	<xsl:param name="CurrentDate"/>

	<xsl:variable name="nameattribute">name</xsl:variable>
	<xsl:variable name="empty"></xsl:variable>
	<xsl:variable name="training">Training</xsl:variable>
	<xsl:variable name="general">General</xsl:variable>

	<xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>
	<xsl:variable name="logphp">http://ubuc.org/maintenance/Log.php</xsl:variable>
	<xsl:variable name="constlink">http://ubuc.org/index.php/club-constitution/</xsl:variable>
	<xsl:variable name="qrgen">http://www.mobile-barcodes.com/qr-code-generator/generator.php?str=http://ubuc.org/maintenance/Manual.php?id</xsl:variable>

	<xsl:template match="/">  
     
    <html>
        <head>
			<!-- J Query script declarations. These enable java script functionality like pop up boxes on mouse over -->
			<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
			<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="./javascript/nhpup_1.1.js"></script>

			<!-- Style section contains CSS layout instructions -->
			<style>   
				<!-- Mouse over pop-up box formatting -->
				#pup {position:absolute;
					z-index:200; /* aaaalways on top*/
					padding: 3px;
					margin-left: 10px;
					margin-top: 5px;
					width: 250px;
					border: 1px solid black;
					background-color: #777;
					color: white;
					font-size: 0.95em;
					}     
			</style>

			<link rel="stylesheet" type="text/css" href="css/UBUCManual.css"></link>
		 
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-61486837-1', 'auto');
				ga('send', 'pageview');
			</script>
        </head>

		<!-- HTML body section displaying the UBUC manual -->
        <body>

			<h1>Procedure Details</h1>
			<p>Details about the selected procedure</p>
			<p><a href="{$homelink}">Back To Contents Page</a></p>
			
			<h2>Editing This Procedure</h2>
			<p>You can edit this procedure online to update it for the next person who needs to do it.</p>
			<ul>
				<xsl:variable name="editlink">http://ubuc.org/maintenance/editProcedure.php?id=<xsl:value-of select="$id"/></xsl:variable>
				<li><a href="{$editlink}">Edit this procedure</a></li>
				<!-- <li><a href=''>Delete this procedure</a></li>-->
			</ul>

			<xsl:for-each select="//procedure[@id=$id]">
				<!-- The tamplates are defined later in this file -->
				<xsl:apply-templates select="." mode="page"/>
			</xsl:for-each>


			<!-- At the end of the document place the hidden text for tool/part and consumable pop-uips-->
			<xsl:for-each select="/manual/parts/partbit">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/tools/tool">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/consumables/consumable">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="/manual/references/ref">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>

			<xsl:for-each select="//item">
				<xsl:apply-templates select="." mode="hidden" />
			</xsl:for-each>
		</body>
    </html>
</xsl:template>
	
<xsl:template match="procedure" mode="level">
      <xsl:choose>
         <xsl:when test="@level='easy'">
            <img src="./Resources/green.jpg" onmouseover="nhpup.popup('This procedure is simple, or forms part of basic training, or the impact of it being done wrong is small so it may be done by any UBUCer');">
            </img>
         </xsl:when>

         <xsl:when test="@level='hard'">
            <img src="./Resources/yellow.jpg" onmouseover="nhpup.popup('This procedure is restricted to authorised or certified individuals');">
            </img>
         </xsl:when>

         <xsl:when test="@level='commercial'">
            <img src="./Resources/red.jpg" onmouseover="nhpup.popup('UBUC does not have the capability of doing this procedure and must pay professionals to do it');">
            </img>
         </xsl:when>

         <xsl:otherwise>?</xsl:otherwise>
      </xsl:choose>
</xsl:template>

<!-- Template to apply for displaying a a procedure -->
<xsl:template match="procedure" mode="page">
	<xsl:variable name="procedure">
		<xsl:value-of select="@id" />
	</xsl:variable>
	
	<A NAME="{$procedure}" />
	
	<h1>
	<xsl:value-of select="@name" />
	</h1>
	<a href="{$homelink}">Back To Contents</a>
	<h2>Details</h2>
	<ul>
		<li>UBUCid=<xsl:value-of select="@id"/></li>
		<xsl:variable name="qrlink"><xsl:value-of select="$qrgen"/>=<xsl:value-of select="@id"/>&amp;barcode=url;</xsl:variable>
		<li><a href="{$qrlink}" onmouseover="nhpup.popup('Click here to see QR code for {@name}');"><img src="Resources/qr.png"/></a></li>
		<li>Category = <xsl:value-of select="@description"/></li>
		<li>Optionality = <xsl:value-of select="@requirement"/></li>
		<li>Because = <xsl:value-of select="@reason"/></li>
		<li>Estimated Cost = <xsl:value-of select="@BudgetEstimate"/></li>
		<li>Frequency in Months = <xsl:value-of select="@frequency"/></li>
		<li>Level = <xsl:value-of select="@level"/></li>
		<li>Estimated time in hrs = <xsl:value-of select="@effort"/></li>
	</ul>
	
	<xsl:variable name="loglink02"><xsl:value-of select="$logphp"/>?proc=<xsl:value-of select="$procedure"/></xsl:variable>
	<h2>Procedure Log <a href="{$loglink02}" onmouseover="nhpup.popup('Click here to record a comment about {@name}');"><img src="Resources/Log.gif"/></a></h2>
	This procedure has been completed on the following occasions.
	<table width="100%" border="1">
		<tr>
			<th width="12%" bgcolor="lightgrey"><b>Date</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Member</b></th>
			<th width="12%" bgcolor="lightgrey"><b>Supplier</b></th>
			<th width="8" bgcolor="lightgrey"><b>Cost</b></th>
			<th bgcolor="lightgrey"><b>Comment</b></th>
		</tr>
	
		<xsl:for-each select="document('../xml/log.xml')/logfile/logentry[string-length(@id)=0 and @procedure=$procedure]">
			<xsl:sort select="@date" order="descending" />
			<xsl:apply-templates select="." mode="intable" />
		</xsl:for-each>
	</table>
	
	<h2>Itemlist</h2>
	<p>This procedure applies to the following items:</p>
	<xsl:apply-templates select="." mode="itemlist" />
	
	<!-- Add the node markdown text to a variable so we can parse it to functions. -->
	<xsl:variable name="NodeMarkdown">
		<xsl:value-of select="."/>
	</xsl:variable>
	
	<!-- Create a list of the required items -->
	<h2>Required Items</h2>
	<p>The following items are referenced in the procedure and will be required to complete it.</p>
	<p>This table is populated automatically with the items that are referenced in the text in the instructions.</p>
	<table width="100%" border="1">
		<tr>        
			<th>Item</th>
			<th>Attributes</th>
			<th>In Stores?</th>
		</tr>
		
		<!-- Add Tools, Parts and Attributes -->
		<xsl:value-of select="php:function('listRequiredItems', $NodeMarkdown)" disable-output-escaping="yes"/>
	</table>
	
	<!-- Add Step Instructions -->
	<h2>Instructions</h2>
	<!-- Call the PHP function to convert the string into HTML -->
	<xsl:value-of select="php:function('parseMarkdown', $NodeMarkdown)" disable-output-escaping="yes"/>
	
	
</xsl:template>

<xsl:template match="supplier|item|procedure|partbit|tool|consumable|ref" mode="text">
     <xsl:variable name="id">
        <xsl:value-of select="@id" />
     </xsl:variable>

     <xsl:variable name="link">href="<xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" />"</xsl:variable>

     <xsl:variable name="fred">&lt;a <xsl:value-of select="$link" disable-output-escaping="yes" />  onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />  &amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; 
      <xsl:value-of select="@name" />
     &lt;/a&gt;</xsl:variable>

     <i>
        <xsl:value-of select="$fred" disable-output-escaping="yes" />
     </i>
  </xsl:template>

<xsl:template match="ref" mode="text">
     <xsl:variable name="img">
        <xsl:value-of select="@img" />
     </xsl:variable>

     <xsl:variable name="uri">
        <xsl:value-of select="@uri" />
     </xsl:variable>

     <xsl:if test="string-length($uri)&gt;0">
        <a href="{$uri}" target="_blank">
        <xsl:value-of select="@id" />

        - 
        <xsl:value-of select="@name" />

        <xsl:value-of select="." />
        </a>
     </xsl:if>

     <xsl:if test="string-length($uri)=0">
     <xsl:value-of select="@id" />

     - 
     <xsl:value-of select="@name" />

     <xsl:value-of select="." />
     </xsl:if>
  </xsl:template>

<xsl:template match="partbit|consumable|tool|ref" mode="hidden">
     <xsl:variable name="parturl">
        <xsl:value-of select="@id" />
     </xsl:variable>

     <div style="display:none;" id="{$parturl}">
        <table width="200" border="1">
           <xsl:if test="@image">
              <xsl:variable name="img">
                 <xsl:value-of select="@image" />
              </xsl:variable>

              <img width="150" src="{$img}" />
           </xsl:if>

           <tbody>
              <xsl:for-each select="(@*)">
                 <xsl:sort select="name()" />
                    <xsl:if test="name() != 'name' and name() != 'uri' and name() != 'image'">
                    <tr>
                       <td>
                          <xsl:value-of select="name()" />
                       </td>

                       <td>
                          <xsl:value-of select="." />
                       </td>
                    </tr>
				 </xsl:if>
              </xsl:for-each>

           </tbody>
        </table>
     </div>
  </xsl:template>

<xsl:template match="item" mode="hidden">
     <xsl:variable name="parturl">
        <xsl:value-of select="@id" />
     </xsl:variable>

     <div style="display:none;" id="{$parturl}">
        <table width="200" border="1">
           <xsl:if test="@image">
              <xsl:variable name="img">
                 <xsl:value-of select="@image" />
              </xsl:variable>

              <img width="150" src="{$img}" />
           </xsl:if>

           <tbody>
              <xsl:for-each select="(@*)">
                 <xsl:sort select="name()" />
              <xsl:if test="name() != 'name' and name() != 'image'">

                    <tr>
                       <td>
                          <xsl:value-of select="name()" />
                       </td>

                       <td>
                          <xsl:value-of select="." />
                       </td>
                    </tr>
                 </xsl:if>
              </xsl:for-each>
           </tbody>
        </table>
     </div>
  </xsl:template>

<!-- Template for printing out all the items for which a particular proceedure is upcoming -->
<xsl:template match="procedure" mode="itemlist">
     	<xsl:variable name="procedurename"><xsl:value-of select="@id" /></xsl:variable>

     	<xsl:for-each select="../item|../itemgroup/item">
        	<xsl:variable name="thisvar"><xsl:value-of select="@id" /></xsl:variable>

		<!-- Setup a variable with the date the proceedure was last complete. Not used after update to display item name rather than date. -->
		<xsl:variable name="lastdone">
		   <xsl:for-each select="document('../xml/log.xml')//logentry[@procedure=$procedurename and @id=$thisvar][last()]">
		      <xsl:value-of select="@date" />
		   </xsl:for-each>
		</xsl:variable>

        	<xsl:variable name="OperationalState">
           	<xsl:value-of select="@status"/>
        	</xsl:variable>

        	<xsl:variable name="OperationalLink">
           	<xsl:choose>
              		<xsl:when test="$OperationalState='broken'">brokenlink</xsl:when>
              		<xsl:when test="$OperationalState='damaged'">damagedlink</xsl:when>
              		<xsl:when test="$OperationalState='niggle'">nigglelink</xsl:when>
              		<xsl:when test="$OperationalState='missing'">lostlink</xsl:when>
              		<xsl:otherwise>link</xsl:otherwise>
           	</xsl:choose>
        	</xsl:variable>

        	<xsl:if test="$OperationalState!='decomissioned'">
        		<xsl:variable name="itemnamea"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id" /></xsl:variable>
        		<xsl:variable name="fred">&lt;a href="<xsl:value-of select="$itemnamea" />" class="<xsl:value-of select="$OperationalLink" />" onmouseover="nhpup.popup($(&amp;#39;#<xsl:value-of select="@id" />&amp;#39;).html(), {&amp;#39;width&amp;#39;:200})"&gt; (<xsl:value-of select="@id" />)&lt;/a&gt;</xsl:variable>
        		<xsl:value-of select="$fred" disable-output-escaping="yes" />,
        	</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- Template for printing out the logentries into the table -->
<xsl:template match="logentry" mode="intable">
	<!-- Display a table row for the an items log -->
	<xsl:variable name="OperationalColour">
		<xsl:choose>
			<xsl:when test="@commenttype='broken'">red</xsl:when>
			<xsl:when test="@commenttype='damaged'">orange</xsl:when>
			<xsl:when test="@commenttype='niggle'">yellow</xsl:when>
			<xsl:when test="@commenttype='missing'">grey</xsl:when>
			<xsl:when test="@commenttype='fixed'">green</xsl:when>
			<xsl:when test="@commenttype='working'">green</xsl:when>
			<xsl:when test="@commenttype='decomissioned'">purple</xsl:when>
			<xsl:otherwise>transparent</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<tr>
		<!-- Display the date column -->
		<td bgcolor="{$OperationalColour}">
			<xsl:value-of select="@date" />
		</td>
		
		<!-- Display the comment author column -->
		<td align="right" bgcolor="{$OperationalColour}">
			<xsl:variable name="Author">
				<xsl:value-of select="@name" />
			</xsl:variable>
			<xsl:value-of select="$Author"/>
		</td>
		
		<!-- Display the supplier name column -->
		<td align="right" bgcolor="{$OperationalColour}">
			<xsl:if test="string-length(@supplier)&gt;0">
				<xsl:variable name="supplierid" select="@supplier"/>
				<xsl:variable name="supplierurl"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="$supplierid" /></xsl:variable>
				<xsl:variable name="suppliername">
					<xsl:for-each select="document('../xml/suppliers.xml')//supplier[@id=$supplierid][last()]">
						<xsl:value-of select="@name" />
					</xsl:for-each>
				</xsl:variable>
				<a href="{$supplierurl}">
					<xsl:value-of select="$suppliername"/>
				</a>
			</xsl:if>
		</td>
		
		<!-- Display the cost column -->
		<td  align="right" bgcolor="{$OperationalColour}">
			<xsl:if test="@price&gt;0">
				<xsl:value-of select="format-number(@price,'####.00')" />
			</xsl:if>
		</td>
		
		<!-- Display the comment column -->
		<td bgcolor="{$OperationalColour}">
			<xsl:variable name="uri">
				<xsl:value-of select="@uri" />
			</xsl:variable>
			<xsl:if test="string-length($uri)&gt;0">
				<a href="{$uri}" target="_blank">
					<xsl:value-of select="." />
				</a>
			</xsl:if>
			<xsl:if test="string-length($uri)=0">
				<xsl:value-of select="." />
			</xsl:if>
		</td>
	</tr>
</xsl:template>

</xsl:stylesheet>