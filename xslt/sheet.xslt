<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:variable name="homelink">http://ubuc.org/maintenance/Manual.php</xsl:variable>
	<xsl:template match="/">

<xsl:text disable-output-escaping="yes"><![CDATA[<?xml-stylesheet type="text/xsl"  href="/Data/websites/ubuc.org/www/wwwroot/maintenance/xslt/SheetDisplay.xslt"?>]]></xsl:text>
  <Worksheet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="/Data/websites/ubuc.org/www/wwwroot/maintenance/xsd/sheet.xsd">
	   
	   
			<xsl:for-each select="//item">
				<Row>
					<name>
						<xsl:value-of select="@name"/>
					</name>

					<id>
					<xsl:variable name="link"><xsl:value-of select="$homelink"/>?id=<xsl:value-of select="@id"/></xsl:variable>

					<a href="{$link}">
						<xsl:value-of select="@id"/>
					</a>
					</id>

					<status>
						<xsl:value-of select="@status"/>
					</status>
					<officers>
						<xsl:for-each select="ancestor::collection[@officer]">
							<xsl:if test="@officer">

								<xsl:value-of select="@officer"/>;</xsl:if>
						</xsl:for-each>
					</officers>
					<otherattributes>
						<xsl:for-each select="(@*)">
							<xsl:sort select="name()"/>

							<xsl:if test="name() != 'id' and name() != 'name' and name() != 'image'">

								<xsl:value-of select="name()"/>=
								<xsl:value-of select="."/>;</xsl:if>
						</xsl:for-each>
					</otherattributes>
			    </Row>
			</xsl:for-each>
	     

			</Worksheet>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\xml\FirstPass.xml" htmlbaseurl="" outputurl="..\sheetoutput.xml" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bGenerateByteCode" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->